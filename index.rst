.. Nemesis documentation master file, created by
   sphinx-quickstart on Mon Oct  5 15:48:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Scientific research

   ./scientific_research/introduction
   ./scientific_research/methods

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Theory

   ./lssem/LSSEM
   ./theory/thermodynamics
   ./dim/DiffuseInterfaceMethods

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Software

   ./nemesis/introduction

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: References

   ./references/zreferences

Documentation by Marcel Kwakkel
===============================

All contents found here is the sole responsibility of Marcel Kwakkel.

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

.. Highlighted options
.. ===================
.. "attention", "caution", "danger", "error", "hint", "important", "note", "tip", "warning", "admonition"

.. links-placeholder

.. include:: ./links.txt
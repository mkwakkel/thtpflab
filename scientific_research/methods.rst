Methods
*******
Analytical, experimental and numerical research methods.
.. https://www.thoughtco.com/random-vs-systematic-error-4175358

.. References
.. ==========

.. toctree::
   :maxdepth: 3

.. links-placeholder

.. include:: ../links.txt
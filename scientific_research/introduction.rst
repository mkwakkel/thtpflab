Introduction
************
Many people might use **hypothesis**, **model**, **theory**, and **law** without considering
their fundamental differences. However, in scientific research each has its own specific meaning,
and it is important to understand these.

.. topic:: Hypothesis

   A hypothesis is based on observations, and therefore a relation you see between cause and effect. 
   By performing more experiments an hypothesis can be supported or proven to be false, but it is 
   never possible to prove it to be *generally* correct.
   *Example*: `Taylor's Hypothesis`_.

.. topic:: Model

   A (conceptual or physical) model is used to *explain* complex phenomena. It is a simplification of
   reality, but it includes all important aspects in order to be able to predict observations.
   *Example*: `Standard Model`_.

.. topic:: Theory

   A (scientific) theory is a hypothesis that has been supported by repeated testing. As long as
   it is not disproven by hard evidence, a theory remains valid. As such, it can be considered
   a generally accepted hypothesis. A theory *explains* the underlying physics.
   *Example*: `Einstein's Theory of Relativity`_.

.. topic:: Law

   A (scientific) law is an *mathematical relation* that *describes* a range of observations 
   (without exceptions). Where a theory *explains*, a law only *desribes* (it does not answer *why?*). 
   Many laws are only valid under limited circumstances, which is why forming new laws is hard nowadays.
   *Example*: `Newton's Laws of Motion`_.

All science is based on empirical observations (facts), which are being analysed to gain 
understanding of physical processes.
There is no *absolute truth*, as anything can (and will be) be overturned if proven false. 
However, note that also the definition of *proof* varies between scientific disciplines.
Most importantly, be aware that these differences between definitions exist (and can not be 
interchanged with each other), and be as accurately as possible within your own discipline.

Famous scientific theories that were disproven later: `Luminiferous aether`_.

.. toctree::
   :maxdepth: 3

.. links-placeholder

.. include:: ../links.txt
Least-Squares spectral element method
*************************************

test

.. toctree::
   :maxdepth: 3

   ./LeastSquaresMethod
   ./BasisFunctions
   ./NumericalIntegration
   ./TransfiniteMapping
   ./MatrixAssembly

.. links-placeholder
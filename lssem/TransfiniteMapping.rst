Transfinite mapping
*******************

By performing a so-called *transfinite mapping*, all operations on basis functions
(p.a. multiplication, differentiation and integration) within a single mesh element
(defined by physical coordinates) can be performed on a *standard element* (defined
by natural coordinates, which are within the range -1 and +1). As a result,  all 
these operations become independent of the specific mesh element. Therefore, this 
approach is more efficient and easier to implement correctly. 

Interpolation within a standard element
=======================================
A standard element is defined as

.. math::
   :label: standard_element
   
   \Omega_{st} = \left\{ \left( \xi, \eta, \zeta \right) | -1 \leq \xi,\eta,\zeta \leq 1 \right\},

where the number of *natural coordinates* ($\xi$, $\eta$, $\zeta$) matches the dimension
of the mesh. In the figure below, the 2D transfinite mapping from the physical to the natural domain 
is visualized.

.. tikz:: 2D transfinite mapping

   \begin{tikzpicture}[scale=3]
   \filldraw[fill=black,dashed]
   (0.5,0.15) circle (0.5pt) node[anchor=north west, scale=0.7] {$\mathbf{v}_1$} --
   (0.9,0.5) circle (0.5pt) node[anchor=west, scale=0.7] {$\mathbf{v}_2$} -- 
   (0.4,0.9) circle (0.5pt) node[anchor=south, scale=0.7] {$\mathbf{v}_3$} --
   (0.1,0.4) circle (0.5pt) node[anchor=north, scale=0.7] {$\mathbf{v}_4$} -- (0.5,0.15);
   \draw [->](0,0) -- (0,1.0);
   \draw [->](0,0)--(1.0,0);

   \node at (0.7,0.325)[scale=0.7,fill=white,opacity=0.5]{$\mathbf{e}_1$};
   \node at (0.625,0.7)[scale=0.7,fill=white,opacity=0.5]{$\mathbf{e}_2$};
   \node at (0.25,0.625)[scale=0.7,fill=white,opacity=0.5]{$\mathbf{e}_3$};
   \node at (0.3,0.275)[scale=0.7,fill=white,opacity=0.5]{$\mathbf{e}_4$};
   
   \node at (1.05,-0.05)[scale=0.7]{$x$};
   \node at (-0.05,1.05)[scale=0.7]{$y$};

   \node at (1.075,0.75)[scale=0.7]{$\Omega$};
   \node at (1.925,0.75)[scale=0.7]{$\Omega_e$};
   \draw[<-] (1.85,0.75) arc (45:135:0.5);

   \filldraw[fill=black,dashed]
   (2.1,0.1) circle (0.5pt) node[anchor=north east, scale=0.7] {$\mathbf{v}_1$} --
   (2.9,0.1) circle (0.5pt) node[anchor=north west, scale=0.7] {$\mathbf{v}_2$} -- 
   (2.9,0.9) circle (0.5pt) node[anchor=south west, scale=0.7] {$\mathbf{v}_3$} --
   (2.1,0.9) circle (0.5pt) node[anchor=south east, scale=0.7] {$\mathbf{v}_4$} -- (2.1,0.1);
   \draw [->](2.0,0.5)--(3.0,0.5);
   \draw [->](2.5,0.0)--(2.5,1.0);

   \node at (2.5,0.1)[scale=0.7,fill=white,opacity=0.5]{$\mathbf{e}_1$};
   \node at (2.9,0.5)[scale=0.7,fill=white,opacity=0.5]{$\mathbf{e}_2$};
   \node at (2.5,0.9)[scale=0.7,fill=white,opacity=0.5]{$\mathbf{e}_3$};
   \node at (2.1,0.5)[scale=0.7,fill=white,opacity=0.5]{$\mathbf{e}_4$};

   \node [scale=.5] at (2.15,0.45) {$-1$};
   \node [scale=.5] at (2.85,0.45) {$+1$};
   \node [scale=.5] at (2.45,0.15) {$-1$};
   \node [scale=.5] at (2.45,0.85) {$+1$};
   \node at (3.05,0.45)[scale=0.7]{$\xi$};
   \node at (2.55,1.05)[scale=0.7]{$\eta$};
   \end{tikzpicture}

Each natural location within the standard element corresponds to a (unique) physical 
location in the mesh element. The *transfinite mapping* is defined as the mapping from 
the physical coordinate to the natural coordinate.

Consider a linear edge $\mathbf{e}_1$ to be defined by two vertices $\mathbf{v}_1$
and $\mathbf{v}_2$. This edge can be parametrized by a natural coordinate $\xi$ 
(which has a range of -1 to +1) as

.. math::
   :label: edge_parametrization
   
   \begin{split}
   \mathbf{e}_1\left( \xi \right) &= \frac{\mathbf{v}_1+\mathbf{v}_2}{2} + \xi \frac{\mathbf{v}_2-\mathbf{v}_1}{2},\\
                                  &= \mathbf{v}_1 \frac{1-\xi}{2} + \mathbf{v}_2 \frac{1+\xi}{2}.
   \end{split}

Similar relations for the other edges can be determined. 
Next, consider a flat face $\mathbf{f}$ to be defined by four edges $\mathbf{e}_1$,
$\mathbf{e}_2$, $\mathbf{e}_3$, and $\mathbf{e}_4$. This face can be parametrized 
by the natural coordinate $\xi$ and $\eta$ (which both have ranges of -1 to +1) as

.. math::
   :label: face_parametrization
   
   \mathbf{f}_5\left( \xi, \eta \right) = \mathbf{e}_1 \frac{1-\eta}{2} + \mathbf{e}_2 \frac{1+\xi}{2} +
                                        \mathbf{e}_2 \frac{1+\eta}{2} + \mathbf{e}_4 \frac{1-\xi}{2} -
   \left\{ \mathbf{v}_1 \frac{1-\xi}{2} \frac{1-\eta}{2} +
   \mathbf{v}_2 \frac{1+\xi}{2} \frac{1-\eta}{2} +
   \mathbf{v}_3 \frac{1+\xi}{2} \frac{1+\eta}{2} +
   \mathbf{v}_4 \frac{1-\xi}{2} \frac{1+\eta}{2} \right\}.

In this last relation, the need for the subtraction of the vertices can be understood after realizing 
that these are added twice by the edge contributions. Again, similar relations for the other faces 
can be determined. 

Finally, a hexahedron (volume) is defined by 6 faces, and its parametrization can be written as

.. math::
   :label: hex_parametrization
   
   \begin{split}
   \mathbf{h}\left( \xi, \eta, \zeta \right) = &\mathbf{f}_1 \frac{1-\xi}{2} + \mathbf{f}_2 \frac{1+\xi}{2} +
                                               \mathbf{f}_3 \frac{1-\eta}{2} + \mathbf{f}_4 \frac{1+\eta}{2} +
                                               \mathbf{f}_5 \frac{1-\zeta}{2} + \mathbf{f}_6 \frac{1+\zeta}{2} \\
        &- \mathbf{e}_1 \frac{1-\eta}{2} \frac{1-\zeta}{2} 
         - \mathbf{e}_3 \frac{1+\eta}{2} \frac{1-\zeta}{2}
         - \mathbf{e}_5 \frac{1-\eta}{2} \frac{1+\zeta}{2}
         - \mathbf{e}_7 \frac{1+\eta}{2} \frac{1+\zeta}{2} \\
        &- \mathbf{e}_2 \frac{1+\xi}{2} \frac{1-\zeta}{2}
         - \mathbf{e}_4 \frac{1-\xi}{2} \frac{1-\zeta}{2}
         - \mathbf{e}_6 \frac{1+\xi}{2} \frac{1+\zeta}{2} 
         - \mathbf{e}_8 \frac{1-\xi}{2} \frac{1+\zeta}{2} \\
        &- \mathbf{e}_9 \frac{1-\xi}{2} \frac{1-\eta}{2} 
         - \mathbf{e}_{10} \frac{1+\xi}{2} \frac{1-\eta}{2}
         - \mathbf{e}_{11} \frac{1+\xi}{2} \frac{1+\eta}{2} 
         - \mathbf{e}_{12} \frac{1-\xi}{2} \frac{1+\eta}{2}\\
        &+ \left\{ \left( \mathbf{v}_1 \frac{1-\xi}{2} \frac{1-\eta}{2} +
                          \mathbf{v}_2 \frac{1+\xi}{2} \frac{1-\eta}{2} +
                          \mathbf{v}_3 \frac{1+\xi}{2} \frac{1+\eta}{2} +
                          \mathbf{v}_4 \frac{1-\xi}{2} \frac{1+\eta}{2} \right) \frac{1-\zeta}{2} +
                   \left( \mathbf{v}_5 \frac{1-\xi}{2} \frac{1-\eta}{2} +
                          \mathbf{v}_6 \frac{1+\xi}{2} \frac{1-\eta}{2} +
                          \mathbf{v}_7 \frac{1+\xi}{2} \frac{1+\eta}{2} +
                          \mathbf{v}_8 \frac{1-\xi}{2} \frac{1+\eta}{2} \right) \frac{1+\zeta}{2} \right\},
   \end{split}

where the first part is the contribution of the faces, the second part a correction to prevent duplication 
of edges, and the third part to ensure a correct contribution of vertices (the first and second part cancel
out the vertices exactly).

Note that the entire parametrization depends on the definition of position/direction of vertices, edges and faces. 
The following definitions are assumed here:

=========  ==================  ==================  ==================
Vertex id  $\xi$ coordinate    $\eta$ coordinate   $\zeta$ coordinate
=========  ==================  ==================  ==================
1           -1                          -1                -1
2           +1                          -1                -1
3           +1                          +1                -1
4           -1                          +1                -1
5           -1                          -1                +1
6           +1                          -1                +1
7           +1                          +1                +1
8           -1                          +1                +1
=========  ==================  ==================  ==================

=======  ==========  ==========  ==================  ========================
Edge id  vertex1 id  vertex2 id  direction/variable  constant values
=======  ==========  ==========  ==================  ========================
1        1                2       $+\xi$             $\eta\equiv-1$, $\zeta\equiv-1$
2        2                3       $+\eta$            $\xi \equiv+1$, $\zeta\equiv-1$
3        4                3       $+\xi$             $\eta\equiv+1$, $\zeta\equiv-1$
4        1                4       $+\eta$            $\xi \equiv-1$, $\zeta\equiv-1$
5        5                6       $+\xi$             $\eta\equiv-1$, $\zeta\equiv+1$
6        6                7       $+\eta$            $\xi \equiv+1$, $\zeta\equiv+1$
7        8                7       $+\xi$             $\eta\equiv+1$, $\zeta\equiv+1$
8        5                8       $+\eta$            $\xi \equiv-1$, $\zeta\equiv+1$
9        1                5       $+\zeta$           $\xi \equiv-1$, $\eta\equiv-1$
10       2                6       $+\zeta$           $\xi \equiv+1$, $\eta\equiv-1$
11       3                7       $+\zeta$           $\xi \equiv+1$, $\eta\equiv+1$
12       4                8       $+\zeta$           $\xi \equiv-1$, $\eta\equiv+1$
=======  ==========  ==========  ==================  ========================

=======  ========  ========  ========  ========  ================  ==============
Face id  edge1 id  edge2 id  edge3 id  edge4 id  normal direction  constant value
=======  ========  ========  ========  ========  ================  ==============
1         4         12        -8         -9       $+\xi$           $\xi\equiv-1$
2         2         11        -6         -10      $+\xi$           $\xi\equiv+1$
3         1         10        -5         -9       $+\eta$          $\eta\equiv-1$
4         3         11        -7         -12      $+\eta$          $\eta\equiv+1$
5         1         2         -3         -4       $+\zeta$         $\zeta\equiv-1$
6         5         6         -7         -8       $+\zeta$         $\zeta\equiv+1$
=======  ========  ========  ========  ========  ================  ==============

Note here that a minus sign indicates an opposite edge direction.

Differentiation within a standard element
=========================================
In partial differential equations (PDEs) the differentials are defined in the physical
domain. In order to use the differentiation within a standard element, a (back) 
transformation from the natural to the physical domain needs to be made. 

1D element
----------
For a 1D element, the first derivative is given by the following chain rule

.. math::
   :label: 1d_gradient_1st
   
   \frac{\partial}{\partial x} = \frac{\partial \xi}{\partial x} \frac{\partial}{\partial \xi},

where the partial derivative $\frac{\partial \xi}{\partial x}$ is just the inverse of 
$\frac{\partial x}{\partial \xi}$. The second derivative is given by

.. math::
   :label: 1d_gradient_2nd
   
   \begin{split}
     \frac{\partial^2}{\partial x^2} &= 
     \frac{\partial \xi}{\partial x} \frac{\partial}{\partial \xi} 
      \left( \frac{\partial \xi}{\partial x} \frac{\partial}{\partial \xi} \right),\\
     &= \left( \frac{\partial \xi}{\partial x} \right)^2 \frac{\partial^2}{\partial \xi^2} + 
        \frac{\partial^2 \xi}{\partial x^2} \frac{\partial}{\partial \xi}
   \end{split}

where the partial derivatives $\frac{\partial \xi}{\partial x}$ and $\frac{\partial^2 \xi}{\partial x^2}$
are just the inverses of $\frac{\partial x}{\partial \xi}$ and $\frac{\partial^2 x}{\partial \xi^2}$, respectively.

2D element
----------
For a 2D element the chain rule gives

.. math::
   :label: 2d_gradient
   
   \nabla = 
    \begin{bmatrix} 
      \frac{\partial}{\partial x} \\
      \frac{\partial}{\partial y} 
    \end{bmatrix}
    =
    \begin{bmatrix} 
      \frac{\partial \xi}{\partial x} \frac{\partial}{\partial \xi} + \frac{\partial \eta}{\partial x} \frac{\partial}{\partial \eta} \\
      \frac{\partial \xi}{\partial y} \frac{\partial}{\partial \xi} + \frac{\partial \eta}{\partial y} \frac{\partial}{\partial \eta}
    \end{bmatrix},

where $\nabla$ is the nable operator. The partial derivative with respect to $x$ and $y$ can be
obtained analytically for a linear mapping, but a more general technique is required for curvilinear
mappings. The partial derivatives with respect to $\xi$ and $\eta$ can be obtained directly from the 
transfinite mapping. The following chain rule can be used for the total change of a general function 
$u(\xi,\eta)$

.. math::
   :label: diff_function

   \textrm{d}u(\xi,\eta) = \frac{\partial u}{\partial \xi} \textrm{d}\xi + \frac{\partial u}{\partial \eta} \textrm{d}\eta.

If the function $u$ is replaced by the mapping $\mathcal{X}(\xi,\eta)$, the following system is obtained

.. math::
   :label: diff_mapping
   
    \begin{bmatrix} 
      \textrm{d}x\\
      \textrm{d}y
    \end{bmatrix}
    =
    \begin{bmatrix} 
      \frac{\partial x}{\partial \xi} & \frac{\partial x}{\partial \eta} \\
      \frac{\partial y}{\partial \xi} & \frac{\partial y}{\partial \eta}
    \end{bmatrix}
    \begin{bmatrix} 
      \textrm{d}\xi\\
      \textrm{d}\eta
    \end{bmatrix}.

This can be inverted to obtain

.. math::
   :label: diff_mapping_inv
   
    \begin{bmatrix} 
      \textrm{d}\xi\\
      \textrm{d}\eta
    \end{bmatrix}
    =
    \frac{1}{\left| J_{\textrm{2D}} \right|}
    \begin{bmatrix} 
      \frac{\partial y}{\partial \eta} & -\frac{\partial x}{\partial \eta} \\
      -\frac{\partial y}{\partial \xi} & \frac{\partial x}{\partial \xi}
    \end{bmatrix}
    \begin{bmatrix} 
      \textrm{d}x\\
      \textrm{d}y
    \end{bmatrix},

where $\left| J_{\textrm{2D}} \right|$ is the 2D Jacobian defined by

.. math::
   :label: Jacobian_2D
   
   \left| J_{\textrm{2D}} \right|
    =
    \begin{vmatrix} 
      \frac{\partial x}{\partial \xi} & \frac{\partial x}{\partial \eta} \\
      \frac{\partial y}{\partial \xi} & \frac{\partial y}{\partial \eta}
    \end{vmatrix}
    = \frac{\partial x}{\partial \xi}\frac{\partial y}{\partial \eta} - \frac{\partial x}{\partial \eta}\frac{\partial y}{\partial \xi}.

As the transfinite mapping is assumed to be one-to-one and have an inverse, the chain rule can also
be applied directly to $\xi$ and $\eta$ to obtain

.. math::
   :label: diff_mapping_inv_direct
   
    \begin{bmatrix} 
      \textrm{d}\xi\\
      \textrm{d}\eta
    \end{bmatrix}
    =
    \begin{bmatrix} 
      \frac{\partial \xi}{\partial x} & \frac{\partial \xi}{\partial y} \\
      \frac{\partial \eta}{\partial x} & \frac{\partial \eta}{\partial y}
    \end{bmatrix}
    \begin{bmatrix} 
      \textrm{d}x\\
      \textrm{d}y
    \end{bmatrix}.

Combining these results leads to the following partial derivatives

.. math::
   :label: partial_derivatives_2D
   
     \frac{\partial \xi}{\partial x} = \frac{1}{\left| J_{\textrm{2D}} \right|} \frac{\partial y}{\partial \eta}, \qquad
     \frac{\partial \xi}{\partial y} = -\frac{1}{\left| J_{\textrm{2D}} \right|} \frac{\partial x}{\partial \eta}, \qquad
     \frac{\partial \eta}{\partial x} = -\frac{1}{\left| J_{\textrm{2D}} \right|} \frac{\partial y}{\partial \xi}, \qquad
     \frac{\partial \eta}{\partial y} = \frac{1}{\left| J_{\textrm{2D}} \right|} \frac{\partial x}{\partial \xi}.

.. math::
   :label: 2d_gradient_complete
   
   \nabla = 
    \begin{bmatrix} 
      \frac{\partial}{\partial x} \\
      \frac{\partial}{\partial y} 
    \end{bmatrix}
    =
    \frac{1}{\left| J_{\textrm{2D}} \right|}
    \underbrace{
    \begin{bmatrix} 
      \frac{\partial y}{\partial \eta} & -\frac{\partial x}{\partial \eta}\\
      -\frac{\partial y}{\partial \xi} & \frac{\partial x}{\partial \xi}
    \end{bmatrix}
    }_{M}
    \begin{bmatrix} 
      D_{\xi}\\
      D_{\eta}
    \end{bmatrix},

where $D_{\xi}$ and $D_{\eta}$ are differential operators of the basis functions. Note that the array 
$M$ becomes the identity matrix if $x$ and $y$ are aligned with $\xi$ and $\eta$, respectively.

A similar derivation for the second derivatives can be made, which leads to

.. math::
   :label: 2d_gradient_2nd_initial
   
   \begin{split}
     \frac{\partial^2}{\partial \xi^2} &= \left( \frac{\partial x}{\partial \xi} \right)^2 \frac{\partial^2}{\partial x^2} +
                                          \left( \frac{\partial y}{\partial \xi} \right)^2 \frac{\partial^2}{\partial y^2} +
                                          2 \frac{\partial x}{\partial \xi} \frac{\partial y}{\partial \xi} \frac{\partial^2}{\partial x \partial y} +
                                          \frac{\partial^2 x}{\partial \xi^2} \frac{\partial}{\partial x} + 
                                          \frac{\partial^2 y}{\partial \xi^2} \frac{\partial}{\partial y},\\
     \frac{\partial^2}{\partial \eta^2} &= \left( \frac{\partial x}{\partial \eta} \right)^2 \frac{\partial^2}{\partial x^2} +
                                          \left( \frac{\partial y}{\partial \eta} \right)^2 \frac{\partial^2}{\partial y^2} +
                                          2 \frac{\partial x}{\partial \eta} \frac{\partial y}{\partial \eta} \frac{\partial^2}{\partial x \partial y} +
                                          \frac{\partial^2 x}{\partial \eta^2} \frac{\partial}{\partial x} + 
                                          \frac{\partial^2 y}{\partial \eta^2} \frac{\partial}{\partial y},\\
     \frac{\partial^2}{\partial \xi \partial \eta} &= \frac{\partial x}{\partial \xi} \frac{\partial x}{\partial \eta} \frac{\partial^2}{\partial x^2} +
                                          \frac{\partial y}{\partial \xi} \frac{\partial y}{\partial \eta} \frac{\partial^2}{\partial y^2} +
                                          \left( \frac{\partial x}{\partial \eta} \frac{\partial y}{\partial \xi} +
                                                 \frac{\partial y}{\partial \eta} \frac{\partial x}{\partial \xi} \right) 
                                                 \frac{\partial^2}{\partial x \partial y} +
                                          \frac{\partial^2 x}{\partial \xi \partial \eta} \frac{\partial}{\partial x} + 
                                          \frac{\partial^2 y}{\partial \xi \partial \eta} \frac{\partial}{\partial y},
   \end{split} 

All derivatives of the physical with respect to the natural space can be derived directly from the
transfinite mapping. However, as the PDEs require derivative operators with respect to physical coordinates,
for practical use this needs to be rewritten as

.. math::
   :label: 2d_gradient_2nd
   
   \begin{Bmatrix} 
     \frac{\partial^2}{\partial^2 x} \\
     \frac{\partial^2}{\partial^2 y} \\
     \frac{\partial^2}{\partial x \partial y} 
   \end{Bmatrix}
    =
    \underbrace{
    \begin{bmatrix} 
      \left( \frac{\partial x}{\partial \xi} \right)^2 & \left( \frac{\partial y}{\partial \xi} \right)^2 & 2 \frac{\partial x}{\partial \xi} \frac{\partial y}{\partial \xi}\\
      \left( \frac{\partial x}{\partial \eta} \right)^2 & \left( \frac{\partial y}{\partial \eta} \right)^2 & 2 \frac{\partial x}{\partial \eta} \frac{\partial y}{\partial \eta}\\
      \frac{\partial x}{\partial \xi} \frac{\partial x}{\partial \eta} & \frac{\partial y}{\partial \xi} \frac{\partial y}{\partial \eta} & \frac{\partial x}{\partial \eta} \frac{\partial y}{\partial \xi} + \frac{\partial y}{\partial \eta} \frac{\partial x}{\partial \xi}
    \end{bmatrix}^{-1}}_{\hat{J_2}}
    \left(
    \begin{Bmatrix} 
      \frac{\partial^2}{\partial^2 \xi} \\
      \frac{\partial^2}{\partial^2 \eta} \\
      \frac{\partial^2}{\partial \xi \partial \eta} 
    \end{Bmatrix}
    - 
    \underbrace{
    \begin{bmatrix} 
      \frac{\partial^2 x}{\partial \xi^2} & \frac{\partial^2 y}{\partial \xi^2}\\
      \frac{\partial^2 x}{\partial \eta^2} & \frac{\partial^2 y}{\partial \eta^2}\\
      \frac{\partial^2 x}{\partial \xi \partial \eta} & \frac{\partial^2 y}{\partial \xi \partial \eta}
    \end{bmatrix}}_{\bar{J_2}}
    \begin{Bmatrix}
      \frac{\partial}{\partial x}\\
      \frac{\partial}{\partial y}
    \end{Bmatrix}
    \right).

This implementation is used in the ``FEHandler`` class in Nemesis, see the listing below:

.. code-block:: matlab
   :linenos:
   :caption: Simplified implementation of the Dxx operator (see the getDxx method in the FEHandler.m class)

    function out = getDxx( self, i_element, use_scaling )
      assert( nargin>1, 'Please provide an element number, or use getDxx_array' )
      if ( nargin == 2 )
        use_scaling = true;
      end
      
      finite_element = self.getFiniteElement( i_element );                                                    % Get the finite element of element i_element
      J_inverse_10 = self.quadrature.Jacobian{ i_element }{ 2 }.inverse_xx_10;                                % \hat{J_2}(1,1)
      J_inverse_01 = self.quadrature.Jacobian{ i_element }{ 2 }.inverse_xx_01;                                % \hat{J_2}(1,2)
      J_inverse_11 = self.quadrature.Jacobian{ i_element }{ 2 }.inverse_xx_11;                                % \hat{J_2}(1,3)
      
      J_2nd_11 = self.quadrature.Jacobian{ i_element }{ 2 }.derivative_11;                                    % \bar{J_2}(1,1)
      J_2nd_12 = self.quadrature.Jacobian{ i_element }{ 2 }.derivative_12;                                    % \bar{J_2}(1,2)
      J_2nd_21 = self.quadrature.Jacobian{ i_element }{ 2 }.derivative_21;                                    % \bar{J_2}(2,1)
      J_2nd_22 = self.quadrature.Jacobian{ i_element }{ 2 }.derivative_22;                                    % \bar{J_2}(1,2)
      J_2nd_31 = self.quadrature.Jacobian{ i_element }{ 2 }.derivative_31;                                    % \bar{J_2}(3,1)
      J_2nd_32 = self.quadrature.Jacobian{ i_element }{ 2 }.derivative_32;                                    % \bar{J_2}(3,2)
      
      % NB: Do not use dofs scaling, as this is done in the end of this function
      Dx = self.getDx( i_element, false );                                                                    % Dx operator (unscaled!)
      Dy = self.getDy( i_element, false );                                                                    % Dy operator (unscaled!)
      
      out = finite_element.zero_matrix;                                                                       % Initialize as a zero matrix
      out = out + ( finite_element.DD{1,1} - ( Dx .* J_2nd_11(:) + Dy .* J_2nd_12(:) ) ) .* J_inverse_10(:);  % DD{1,1} = \frac{\partial^2}{\partial^2 \xi}
      out = out + ( finite_element.DD{2,2} - ( Dx .* J_2nd_21(:) + Dy .* J_2nd_22(:) ) ) .* J_inverse_01(:);  % DD{2,2} = \frac{\partial^2}{\partial^2 \eta}
      out = out + ( finite_element.DD{1,2} - ( Dx .* J_2nd_31(:) + Dy .* J_2nd_32(:) ) ) .* J_inverse_11(:);  % DD{1,2} = \frac{\partial^2}{\partial \xi \partial \eta}

      if ( use_scaling )
        out = out * self.dofs.scaling.elemental{ i_element };                                                 % Scaling of dofs, which necessary for AMR
      end
    end
Multi-element description
*************************

If a domain is divided into multiple elements, the contribution of each *mesh element*
must be taken into account. This can be done by a *global matrix assembly*, or by an
*element-by-element* technique. Both approaches will be discussed here, however 
specific assembly choices will only be discussed for the global matrix assembly case. To 
illustrate the consequence of assembly choices, a 1D mesh with only 3 elements will be 
considered. Unless stated otherwise, each mesh element has a Lagrangian basis function (C0) 
of order 4, which are defined at 4 quadrature points. In that case the H-operator of each
mesh element is a identity matrix.



Global matrix assembly
----------------------

Basis functions
***************

Describe how basis functions are formed (1D), and how to use them in multiple dimensions.

#. Model vs. nodal basis functions
#. Lagrange polynomials
#. GLL points
#. Reference domain (-1,1)

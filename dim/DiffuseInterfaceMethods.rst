.. role:: todo

Diffuse interface methods
*************************
The phase-field model for a two-phase system is based on the
definition of a phase-field parameter :math:`C`, which attains values between
limiting :math:`C` values. Away from the interface, :math:`C` attains these limiting
values, while there is a smooth transition between these limiting values around 
the interface. A phase-field method defines an energy functional that is commonly 
the following Helmholtz (free) energy functional

.. math::
   :label: helmholtzFunctional

   \mathcal{F} = \int_{\Omega} f \, \text{d}V = \int_{\Omega} \frac{\sigma}{\epsilon}  \left( \Psi(C) + \frac{\epsilon^2}{2} \left(\nabla C \right)^2\right) \text{d}V,

where :math:`f` is the Helmholtz energy density, :math:`\sigma` denotes the surface tension 
coefficient, and :math:`\epsilon` determines the width of the diffuse interface. 
The two terms in the energy functional account for the *bulk energy density* and the 
*interfacial energy density*, respectively. The latter is the excess energy due to an 
inhomogeneous distribution of :math:`C` in the interfacial region.
The phobic bulk energy density :math:`\Psi(C)` is a double-well potential
This double-well potential has minima at :math:`C_{min}` and :math:`C_{max}`, and 
tries to sharpen the interface by separating the phases. On the other hand, the 
philic gradient term tries to diffuse the interface into the bulk regions. At minimum energy both 
terms balance, and the interface profile reaches its equilibrium shape. Another way to 
interpret this minimum energy is through the *chemical potential* $\omega$, which 
is given by the *functional derivative* of the energy density :eq:`helmholtzFunctional` 
with respect the phase-field $C$. The `functional derivative`_ is a generalization of the 
`Euler-Lagrange equation`_, and it can be written as

.. math::
   :label: omega

   \omega = \frac{\partial f}{\partial C} = \frac{\partial f}{\partial C} - \nabla \cdot \frac{\partial f}{\partial \nabla C}.

Once this chemical potential has becomes uniform in space, the energy density has reached its minimum.
It is important to note that the original works by Van der Waals :cite:`van_der_waals_thermodynamic_1893` 
and Cahn and Hilliard :cite:`cahn_free_1958,cahn_free_1959,cahn_free_1959-1`
corresponds to a one dimensional case (thereby assuming a flat interface). In that case it holds 
that $\nabla C=dC/dz$ and $\Delta C=d^2C/dz^2$, where $z$ is a spatial location.
Later :todo:`[when? by who?]` this was extended to higher dimensions, which usually
leads to the following $\omega_c$ (where $c$ signifies classic approach)

.. math::
   :label: omega_c

   \omega_c = \frac{\partial f}{\partial C} = \frac{\sigma}{\epsilon} \left(\frac{\partial \Psi}{\partial C} - \epsilon^2 \Delta C \right).

As intended, when applied to one-dimensional problems, this chemical potential enforces the balance between the 
sharpening and diffusing terms while conserving (enclosed) mass. At the same time, it is well known that this 
chemical potential can cause nonphysical (enclosed) mass loss and bulk diffusion when applied to higher-dimensional 
problems. Therefore, a substantial amount of studies have tried to remedy these nonphysical effects in various ways
:cite:`biben_tumbling_2003,yang_numerical_2006,nestler_phase-field_2008,hu_total_2019,wang_mass-conserved_2015,li_phase-field_2016,zhang_flux-corrected_2017,soligo_2019,wang_re-initialization_2018,yue_phase-field_2006`. 
Although some of these remedies appear to suppress the nonphysical effects, so far no theoretical cause and
prevention of them has been put forward :cite:`garcke_curvature_2013`.
However, it is possible to attribute the (enclosed) mass loss and bulk diffusion directly to the extension 
of the original one-dimensional formulation (corresponding to a flat interface) to higher dimensions. 

Without loss of generality, it can be assumed that the extension of the energy functional 
to higher dimensions should consider the gradient of the single phase-field $C$ normal to the interface.
The classic Helmholtz energy functional of :eq:`helmholtzFunctional` is then redefined to

.. math::
   :label: helmholtzFunctional_balanced

   \mathcal{F}_b = \int_{\Omega} f_b \, \text{d}V = \int_{\Omega} \frac{\sigma}{\epsilon} \left( \Psi(C) + \frac{\epsilon^2}{2} \left( \nabla_n C \right)^2  \right) \text{d}V,

where $\nabla_n$ is the gradient normal to the interface. The fundamental difference 
is that here $\nabla_n C$ (a scalar) is used, compared to $\nabla C$ (a vector) in the classic 
Helmholtz energy functional of :eq:`helmholtzFunctional`. This subtle modification of the 
energy functional leads to the following balanced chemical potential

.. 
   (see \appref{ap:derivationBalancedPotential} for its derivation)

.. math::
   :label: omegaBalanced
   
   \omega_b = \frac{\partial f_b}{\partial C} = \frac{\sigma}{\epsilon} \left( \frac{\partial \Psi}{\partial C} - \epsilon^2 \frac{\partial^2C}{\partial n^2}\right),

which is identically zero for an interface (profile) at equilibrium (corresponding to :eq:`C_function`). 

.. 
   It can be shown, through a similar analysis as before, that even for curved equilibrium interfaces 
   $\nabla \omega_b \cdot \mathbf{n}_{\Gamma} \equiv 0$, which proofs that the balanced chemical potential does not cause any mass loss and bulk diffusion.

During the derivation of the classic chemical potential :eq:`omegaClassic`, the divergence of 
$\nabla C$ leads to an additional curvature term. This can be understood by considering the difference 
between the two chemical potentials

.. math::
   :label: deltaChemicalPotential

   \omega_c - \omega_b = - \sigma \epsilon \left( \Delta C - \frac{\partial^2 C}{\partial n^2} \right) = \sigma \kappa \epsilon \left| \nabla C \right|,

where $\kappa$ is the curvature. This relation also reflects that mass loss and bulk diffusion
for the classic chemical potential only vanish if $\epsilon \to 0$, the so-called sharp interface 
limit :cite:`jacqmin_calculation_1999`.
From a physical perspective, minimization of the redefined Helmholtz energy functional effectuates
only the recovery of the equilibrium interface profile. This dynamics corresponds to the original 
one-dimensional formulation, and does in general not depend on the interface thickness $\epsilon$. 
On the other hand, minimization of the classic Helmholtz energy functional effectuates a balance 
between the recovery of the equilibrium interface profile and an uniform distribution of the right 
hand side term of :eq:`deltaChemicalPotential`. However, neither of these goals is 
accomplished completely, and the overall dynamics strongly depends on the interface thickness $\epsilon$. 

This can be understood better by combining the difference between the chemical potentials 
:eq:`deltaChemicalPotential` with the uncoupled (from the NS equation) CH equation :eq:`CH`.
For a curved interface with an equilibrium hyperbolic tangent profile and constant $M_C$ this gives

.. math::
   :label: CH_curvatureEffect

   \frac{\partial C}{\partial t} = M_C \sigma \epsilon \Delta \left( \kappa \left| \nabla C \right| \right),
  
which shows that, in order to make $\kappa \left| \nabla C \right|$ more uniform in space, 
$C$ (and therefore the hyperbolic tangent interface profile) will change.
Based on :eq:`csf`, $\kappa \left| \nabla C \right|$ can be considered as a pressure 
jump across (or a surface tension force at) the interface in normal direction. From a 
physical perspective, this interface normal (pressure) term is balanced by surface tension 
forces parallel to the interface (through the Young-Laplace equation), and must not be diffused. 
Therefore, as the use of this additional diffusion term is physically problematic, it 
should be removed. The proposed balanced chemical potential does just that, while 
respecting the original one-dimensional dynamics. As the multi-dimensional balanced energy 
functional is essentially identical to the one-dimensional version, application of the 
energy argument :cite:`yue_spontaneous_2007` yields an equilibrium for the hyperbolic 
tangent profile given by :eq:`C_function` without any need for droplet shrinkage.
Moreover, by removing the source of the mass loss and bulk diffusion the need for 
correction terms or special settings has vanished.

Equilibrium interface profile
-----------------------------
The equilibrium phase-field :math:`C` can be written as a function of the signed 
distance function :math:`d(\mathbf{x})` between the interface and the location :math:`\mathbf{x}`.
In its most general form this results in

.. math::
  :label: C_function

  C(\mathbf{x}) = a \left( b + \tanh \left( \frac{d(\mathbf{x})}{\gamma} \right) \right),
  
where :math:`a` and :math:`b` are constant values, and :math:`\gamma` defines a scaled thickness
of the diffuse interface (denoted by :math:`\epsilon`). The constant values are determined by the defined limiting :math:`C` 
values, and common settings are:

.. list-table:: Common settings to define the diffuse interface
   :widths: 10 20 10 10 10
   :header-rows: 0
   :align: center

   * - option
     - range of :math:`C`
     - :math:`a`
     - :math:`b`
     - :math:`\gamma`
   * - A
     - :math:`-1 \le C \le 1`
     - 1
     - 0
     - :math:`\sqrt{2} \epsilon`
   * - B
     - :math:`\phantom{-}0 \le C \le 1`
     - 1/2
     - 1
     - :math:`2 \sqrt{2} \epsilon`

From a physical perspective both options are identical, however the bulk energy density 
and some scaling parameters will differ.

There is a strong relation between the Level-Set (LS) and the phase-field function :math:`C`.
The LS is generally defined as a signed distance function, so essentially it is equivalent to 
the :math:`d(\mathbf{x})`. By rewriting :eq:`C_function` and application of the 
`inverse hyperbolic tangent function`_, the following derivation can be obtained

.. math::
   :label: LS_function
   
   \begin{split}
    \frac{C(\mathbf{x})}{a} - b &= \tanh \left( \frac{d(\mathbf{x})}{\gamma} \right) \notag \\
    \frac{1}{2} \ln \left( \frac{1 + \frac{C(\mathbf{x})}{a} - b}{1 - \frac{C(\mathbf{x})}{a} + b} \right) &= \frac{d(\mathbf{x})}{\gamma} \notag \\
    d( \mathbf{x} ) = LS( \mathbf{x} ) &= \frac{\gamma}{2} \ln \left( \frac{1 + \frac{C(\mathbf{x})}{a} - b}{1 - \frac{C(\mathbf{x})}{a} + b} \right).
   \end{split}

For the two options A and B this results in

.. list-table:: Relation between phase-field function :math:`C` and the Level-Set function
   :widths: 10 20 30 30
   :header-rows: 0
   :align: center

   * - option
     - range of :math:`C`
     - :math:`C(\mathbf{x})`
     - :math:`LS(\mathbf{x})`
   * - A
     - :math:`-1 \le C \le 1`
     - :math:`\phantom{\frac{1}{2} + \frac{1}{2}} \tanh \left( \frac{d(\mathbf{x})}{\sqrt{2} \epsilon} \right)`
     - :math:`\frac{\epsilon}{\sqrt{2}} \ln \left( \frac{1+C(\mathbf{x})}{1-C(\mathbf{x})} \right)`
   * - B
     - :math:`\phantom{-}0 \le C \le 1`
     - :math:`\frac{1}{2} + \frac{1}{2} \tanh \left( \frac{d(\mathbf{x})}{2\sqrt{2} \epsilon} \right)`
     - :math:`\sqrt{2} \epsilon \ln \left( \frac{C(\mathbf{x})}{1 - C(\mathbf{x})} \right)`


Bulk energy density
-------------------
Depending on the chosen `equilibrium interface profile`_ (option A or B), the bulk energy density
as defined in :eq:`energy_functional` is defined by

.. list-table:: Bulk energy density functions
   :widths: 10 20 30
   :header-rows: 0
   :align: center

   * - option
     - range of :math:`C`
     - energy density function :math:`\Psi`
   * - A
     - :math:`-1 \le C \le 1`
     - :math:`(C+1)^2 (1-C)^2`
   * - B
     - :math:`\phantom{-}0 \le C \le 1`
     - :math:`\frac{1}{4} C^2 (1-C)^2`

Phase-field evolution in time
-----------------------------
The evolution of a phase-field :math:`C(\mathbf{x},t)` is mathematically given by the following 
conservation equation

.. math::
   :label: advectionDiffusion

   \frac{\partial C}{\partial t} + \nabla \cdot \left( \mathbf{u} C \right) = -\nabla_{\mathcal{H}} f(C),

where the advection is controlled by the fluid velocity :math:`\mathbf{u}`
and the right hand side is given by a so-called gradient flow of the energy density $f(C)$.
For :math:`\mathbf{u}=0` the uncoupled (from the NS equation) phase-field equation is obtained, 
where the evolution of $C$ is only affected by the term on the right hand side.
Here :math:`\nabla_{\mathcal{H}}` denotes the (Hilbert space) gradient of :math:`f` at :math:`C`,  
which ensures that :math:`C` decreases along the gradient of :math:`f` on :math:`\mathcal{H}`. 
The precise expression for :math:`\nabla_{\mathcal{H}}` can be found 
from a thermodynamic perspective :cite:`novick-cohen_nonlinear_1984` or by 
a variational formulation (see \appref{ap:gradientFlow}). 
Two well-known results are the Allen-Cahn (AC) equation (so-called :math:`L^2` gradient flow)

.. math::
   :label: AC

   \frac{\partial C}{\partial t} + \nabla \cdot \left( \mathbf{u} C \right) = -M_A \omega,

and the Cahn-Hilliard (CH) equation (so-called $H^{-1}$ gradient flow)

.. math::
   :label: CH

   \frac{\partial C}{\partial t} + \nabla \cdot \left( \mathbf{u} C \right) = \nabla \cdot \left( M_C \nabla \omega \right).

Here $M$ is the interface mobility (often set to unity or a function of $C$), $\omega$ the chemical potential,
and the subscripts $A$ and $C$ denote Allen-Cahn and Cahn-Hilliard, respectively.

:eq:`omega` reduces to

.. math::
   :label: omegaClassic

   \omega_c =  \frac{\sigma}{\epsilon}  \left( C^3 - \frac{3}{2}C^2+ \frac{1}{2} C - \epsilon^2 \Delta C \right).

.. toctree::
   :numbered:
   :maxdepth: 2

.. include:: ../links.txt
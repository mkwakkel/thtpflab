import matplotlib.pyplot as plt
import numpy as np
from itertools import compress 
import phasefield as pf

fig = plt.figure(figsize=(20, 12))

epsilon = 0.05

with plt.xkcd():
    pf.plotA( plt, epsilon )
    
fig.canvas.draw()
plt.show()
# import matplotlib.pyplot as plt
# import networkx as nx


# options = {"node_color": "C0", "node_size": 100}

# G = nx.grid_2d_graph(5, 5)
# plt.subplot(111)
# nx.draw_spectral(G[1,1], **options)

# G = nx.grid_2d_graph(8, 8)
# nx.draw_spectral(G, **options)

# plt.show()

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection

def Hermite( indices, x, J ):
  for n in indices:
    if n==1:
      y= 1/2-3/4*x+1/4*x^3
    elif n==2:
      y=(1/J)*(1/4-1/4*x-1/4*x^2+1/4*x^3)
    elif n==3:
      y= 1/2+3/4*x-1/4*x^3
    elif n==4:
      y= (1/J)*(-1/4-1/4*x+1/4*x^2+1/4*x^3)
    elif n==5:
      y= sqrt(5/128)*(1-x^2)^2
    elif n==6:
      y=sqrt(7/128)*(1-x^2)^2*x
  return

x, y = np.meshgrid(np.linspace(-1,1,5), np.linspace(-1, 1, 5))
segs1 = np.stack((x[:,[0,-1]],y[:,[0,-1]]), axis=2)
segs2 = np.stack((x[[0,-1],:].T,y[[0,-1],:].T), axis=2)

x, y = np.meshgrid(np.linspace(-1,1,5), np.linspace(-1, 1, 5))
segs1_r = np.stack((x[:,[0,-1]],y[:,[0,-1]]), axis=2)
segs2_r = np.stack((x[[0,-1],:].T,y[[0,-1],:].T), axis=2)

an = np.linspace(0, 2 * np.pi, 100)

fig, axs = plt.subplots(2, 2)

# axs[0, 0].plot(3 * np.cos(an), 3 * np.sin(an))
axs[0, 0].add_collection(LineCollection(np.concatenate((segs1, segs2))))
axs[0, 0].set_title('mesh before refinement', fontsize=10)
axs[0, 0].grid()
axs[0, 0].axis('equal')

x = 

axs[0, 1].plot(3 * np.cos(an), 3 * np.sin(an))
axs[0, 1].axis('equal')
axs[0, 1].set_title('equal, looks like circle', fontsize=10)

axs[1, 0].plot(3 * np.cos(an), 3 * np.sin(an))
axs[1, 0].axis('equal')
axs[1, 0].set(xlim=(-3, 3), ylim=(-3, 3))
axs[1, 0].set_title('still a circle, even after changing limits', fontsize=10)

axs[1, 1].plot(3 * np.cos(an), 3 * np.sin(an))
axs[1, 1].set_aspect('equal', 'box')
axs[1, 1].set_title('still a circle, auto-adjusted data limits', fontsize=10)

fig.tight_layout()

plt.show()







# plt.autoscale()
# plt.show()
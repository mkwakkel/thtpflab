import matplotlib.pyplot as plt
import matplotlib.tri as tri
from mpl_toolkits.mplot3d import axes3d, Axes3D
import numpy as np
from itertools import compress 

# converts quad elements into tri elements
def quads_to_tris(quads):
    tris = [[None for j in range(3)] for i in range(2*len(quads))]
    for i in range(len(quads)):
        j = 2*i
        n0 = quads[i][0]
        n1 = quads[i][1]
        n2 = quads[i][2]
        n3 = quads[i][3]
        tris[j][0] = n0
        tris[j][1] = n1
        tris[j][2] = n2
        tris[j + 1][0] = n2
        tris[j + 1][1] = n3
        tris[j + 1][2] = n0
    return tris

# plots a finite element mesh
def plot_fem_mesh(nodes_x, nodes_y, elements):
    for element in elements:
        x = [nodes_x[element[i]] for i in range(len(element))]
        y = [nodes_y[element[i]] for i in range(len(element))]
        plt.fill(x, y, edgecolor='black', fill=False)

def plot_fem_mesh_2D(ax,nodes_x, nodes_y, elements, linecolor, linestyle):
    for element in elements:
        x = [nodes_x[element[i]] for i in range(len(element))]
        y = [nodes_y[element[i]] for i in range(len(element))]
        ax.plot(x, y, color=linecolor, linestyle=linestyle)

def plot_fem_mesh3D(ax,nodes_x, nodes_y, nodes_z, elements, linecolor, linestyle):
    for element in elements:
        x = [nodes_x[element[i]] for i in range(len(element))]
        y = [nodes_y[element[i]] for i in range(len(element))]
        z = [nodes_z[element[i]] for i in range(len(element))]
        ax.plot(x, y, z, color=linecolor, linestyle=linestyle)


def plot_vertices_2D(ax,nodes_x, nodes_y, elements, edgecolors, facecolors):
    for element in elements:
        x = [nodes_x[element[i]] for i in range(len(element))]
        y = [nodes_y[element[i]] for i in range(len(element))]
        ax.scatter(x, y, s=40, edgecolors=edgecolors, facecolors=facecolors,zorder=10)

def plot_vertices(ax,nodes_x, nodes_y, nodes_z, elements, edgecolors, facecolors):
    for element in elements:
        x = [nodes_x[element[i]] for i in range(len(element))]
        y = [nodes_y[element[i]] for i in range(len(element))]
        z = [nodes_z[element[i]] for i in range(len(element))]
        ax.scatter(x, y, z, s=40, edgecolors=edgecolors, facecolors=facecolors)

def plot_vertex_dofs(ax,nodes_x, nodes_y, nodes_z,elements):
    for j in range(len(nodes_x)):
        x = [nodes_x[j] for i in range(len(element))]
        y = [nodes_y[j] for i in range(len(element))]
        z = [nodes_z[j] for i in range(len(element))]
        ax.plot(x, y, z, color=linecolor, linestyle=linestyle)


def add_edge_ids(ax,nodes_x, nodes_y, nodes_z,elements):
    for idx,element in enumerate(elements):
        x = [nodes_x[element[i]] for i in range(len(element))]
        y = [nodes_y[element[i]] for i in range(len(element))]
        z = [nodes_z[element[i]] for i in range(len(element))]
        ax.text(np.mean(x),np.mean(y),np.mean(z),idx)

def add_face_ids(ax,nodes_x, nodes_y, nodes_z,elements,element_tag,indices):
    for idx,element in enumerate(elements):
        x = [nodes_x[element[i]] for i in range(len(element))]
        y = [nodes_y[element[i]] for i in range(len(element))]
        z = [nodes_z[element[i]] for i in range(len(element))]
        tag = element_tag+str(indices[idx])
        ax.text(np.mean(x),np.mean(y),np.mean(z),tag)

def plot_face_2D(ax,nodes_x,nodes_y,elements):
    for idx,element in enumerate(elements):
        x = [nodes_x[element[i]] for i in range(len(element))]
        y = [nodes_y[element[i]] for i in range(len(element))]
        ax.fill(x,y,color='red',alpha=0.1)

def plot_face_3D(ax,nodes_x,nodes_y,nodes_z,elements):
    for idx,element in enumerate(elements):
        x = [nodes_x[element[i]] for i in range(len(element))]
        y = [nodes_y[element[i]] for i in range(len(element))]
        z = [nodes_z[element[i]] for i in range(len(element))]
        xx, yy = np.meshgrid(x, y)
        zz, pp = np.meshgrid(z, z)
        ax.plot_surface(xx,yy,zz,color='red',alpha=0.1)

def add_dof_numbers(ax,nodes_x,nodes_y,elements,dof_numbers):
    for idx,element in enumerate(elements):
        x    = [nodes_x[element[i]] for i in range(len(element))]
        y    = [nodes_y[element[i]] for i in range(len(element))]
        
        [xp,yp] = get_locations(x,y,[5,5])

        #dofs = [dof_numbers[0] for i in range(len(element))]

        xp = xp.flatten(); yp = yp.flatten()

        for point in range(len(xp)):
            ax.text(xp[point],yp[point],dof_numbers[idx][point],horizontalalignment='center',verticalalignment='center')

# Function to return a face dependent meshgrid (physical coordinates)
# which indicate where the dof numbers should be placed
def get_locations(x,y,dims):
    xi  = (2*(np.arange(dims[0]))+1)/dims[0] - 1
    eta = (2*(np.arange(dims[1]))+1)/dims[1] - 1
    [xl,yl] = np.meshgrid(xi,eta)

    # NB: it is assumed that all edges are equal and aligned with the xy-directions!
    #     Add a mapping to make this function more general 
    xmin = min(x); xmax = max(x)
    ymin = min(y); ymax = max(y)

    xp = xmin + (xl+1)/2 * (xmax-xmin)
    yp = ymin + (yl+1)/2 * (ymax-ymin)

    return xp,yp

# FEM data
nodes_x      = [0.0, 1.0, 2.0, 0.0, 1.0, 2.0, 1.0, 1.5, 2.0, 1.0, 1.5, 2.0, 1.0, 1.5, 2.0]
nodes_y      = [0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.5, 1.0, 1.0, 1.0]
nodes_z      = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]

all_vertices    = [[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14]]
active_vertices = np.array( [1,1,0,1,1,0,0,1,1,0,1,1,0,1,1],dtype='bool' )

nodal_values = np.zeros( np.size( nodes_x ) )

edges_L0     = [[0,1],[1,2],[3,4],[4,5],[0,3],[1,4],[2,5]]
edges_L1     = [[6,7],[7,8],[9,10],[10,11],[12,13],[13,14],[6,9],[9,12],[7,10],[10,13],[8,11],[11,14]]
all_edges    = edges_L0 + edges_L1
active_edges = np.ones(np.size(all_edges),dtype='bool' )
active_edges[1] = False
active_edges[3] = False
active_edges[6] = False
active_edges[13] = False
active_edges[14] = False

node_connections    = [[1,6],[2,8],[5,14],[4,12]]

faces_L0  = [[0, 1, 4, 3], [1, 2, 5, 4]]
faces_L1  = [[6, 7,10, 9], [7,8,11,10], [9,10,13,12], [10,11,14,13]]
all_faces = faces_L0 + faces_L1

elements_all_tris = quads_to_tris(all_faces)

# create an unstructured triangular grid instance
triangulation = tri.Triangulation(nodes_x, nodes_y, elements_all_tris)
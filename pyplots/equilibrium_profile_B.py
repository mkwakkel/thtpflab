import matplotlib.pyplot as plt
import numpy as np
from itertools import compress 
import phasefield as pf

fig = plt.figure(figsize=(20, 12))

epsilon = 0.1

with plt.xkcd():
    pf.plotB( plt, epsilon )
    
fig.canvas.draw()
plt.show()
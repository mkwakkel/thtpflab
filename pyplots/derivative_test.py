from pylab import *
import scipy as sp
import numpy as np
from scipy import misc
from sympy import symbols, diff

ax = subplot(111)

x, y = symbols('x y', real=True)

epsilon = 0.1
C = 0.5* (1 + sympy.tanh( np.log(10)*x/epsilon ))
diff(C,x)

def C_function(x):
  return 0.5* (1 + np.tanh( np.log(10)*x/epsilon ))

x = arange(-0.25, 0.25, 0.01)

epsilon = 0.1

y = C_function(x)

plot(x, y,'r-')

yp = misc.derivative( C_function, x)
plot(x, yp,'b-')

grid(True)

ax.spines['left'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['bottom'].set_position('zero')
ax.spines['top'].set_color('none')

text(-0.75, 6.0,
     r'$f(x)=3x^2+2x+1$', horizontalalignment='center',
     fontsize=18,color='red')

text(-1.0, -8.0,
     r"$f'(x)=6x+2$", horizontalalignment='center',
     fontsize=18,color='blue')

show()
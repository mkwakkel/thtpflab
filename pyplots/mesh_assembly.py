import matplotlib.pyplot as plt
import numpy as np
import nemesis as nemesis

def qp_element(i,x,qp):
  dx = x[i]-x[i-1]
  qp_element = x[i-1] + dx * (qp+1)/2
  return qp_element

fig = plt.figure(figsize=(20, 10))

num_modes = 5

periodic  = True

dof_y_position = 1.05

# Determine the GLL points & weights
num_modes_quadrature = num_modes + 0
[qp,w] = nemesis.GaussLobattoLegendre(num_modes_quadrature)

# Set start/end points of elements
xmin = 0; xmax = 1
ymin = 0; ymax = 1
ymin2 = -8; ymax2 = 8
num_elements = 3
x = np.linspace(xmin,xmax, num=num_elements+1, endpoint=True).reshape( num_elements+1, 1 )

#f = np.cos( np.pi * qp )

# Initialize the highres operators
qp_hr = np.linspace(-1,1, num=100, endpoint=True).reshape( 100, 1 )
H_hr = np.zeros( (np.size(qp_hr),num_modes) )
H_hr = np.array(H_hr)
for mode in range(0,num_modes):
  H_hr[ :, mode:mode+1 ] = nemesis.LagrangeGLL(mode,qp_hr,num_modes)
D_hr = nemesis.LagrangeDerivativeMatrix_GLL2(qp_hr,num_modes)

with plt.xkcd():
  plt.suptitle('Basis functions: Lagrange (C0)')
  plt.subplots_adjust(top=0.925,bottom=0.05,left=0.05,right=0.95,hspace=0.1,wspace=0.1)

  #####################################
  # Subplot 1
  ax = plt.subplot(2, 1, 1)
  ax.set_xlim([xmin-0.1,xmax+0.1])
  ax.set_ylim([ymin-0.3,ymax+0.2])

  for i_points in range(0,num_elements+1):
    plt.axvline(x = x[i_points], color = 'k', linestyle=':', label = 'axvline - full height') 

  dof_number = 0

  for i_element in range(0,num_elements):
    # Plot the element lines
    plt.plot( x[i_element:i_element+2], [0,0], color='k', linestyle='-' )

    # Plot modes & dof numbers
    for mode in range(0,num_modes):
      y_hr = H_hr[:,mode]
      x_hr = qp_element(i_element+1,x,qp_hr)

      if mode==0 or mode==num_modes-1:
        ls = '-'
      else:
        ls = '--'

      plt.plot( x_hr, y_hr, color='r', linestyle=ls )
      # plt.plot((qp+1)/2,0*qp,'ko')

    # plot quadrature points
    qpoints = qp_element(i_element+1,x,qp)
    plt.plot( qpoints[0 ],0, 'ks' )                   # start point
    plt.plot( qpoints[1:-1],0*qpoints[1:-1], 'ko' )   # center points
    plt.plot( qpoints[-1:],0, 'ks' )                  # end point

    # Add dof numbering
    # start point (only for 1st element)
    if i_element==0:
      dof_number += 1
      ax.annotate(str(dof_number), xy=(qpoints[0], dof_y_position),xycoords='data',
            horizontalalignment='center', verticalalignment='center',bbox=dict(boxstyle="round", fc="w")
            )

    # end point
    if (periodic and i_element==num_elements-1):
      ax.annotate(str(1), xy=(qpoints[-1], dof_y_position),xycoords='data',
          horizontalalignment='center', verticalalignment='center',bbox=dict(boxstyle="round", fc="w")
          )
    else:
      dof_number += 1
      ax.annotate(str(dof_number), xy=(qpoints[-1], dof_y_position),xycoords='data',
          horizontalalignment='center', verticalalignment='center',bbox=dict(boxstyle="round", fc="w")
          )

    # middle points
    for i_point in range (0,num_modes_quadrature-2):
      dof_number += 1
      ax.annotate(str(dof_number), xy=(qpoints[i_point+1], dof_y_position),xycoords='data',
          horizontalalignment='center', verticalalignment='center',bbox=dict(boxstyle="round", fc="w")
          )
  
  #####################################
  # Subplot 2
  ax = plt.subplot(2, 1, 2)
  ax.set_xlim([xmin-0.1,xmax+0.1])
  ax.set_ylim([ymin2-0.3,ymax2+0.2])

  for i_points in range(0,num_elements+1):
    plt.axvline(x = x[i_points], color = 'k', linestyle=':', label = 'axvline - full height') 

  dof_number = 0

  for i_element in range(0,num_elements):
    # Plot the element lines
    plt.plot( x[i_element:i_element+2], [0,0], color='k', linestyle='-' )

    # Plot modes & dof numbers
    for mode in range(0,num_modes):
      y_hr = D_hr[:,mode]
      x_hr = qp_element(i_element+1,x,qp_hr)

      if mode==0 or mode==num_modes-1:
        ls = '-'
      else:
        ls = '--'

      plt.plot( x_hr, y_hr, color='r', linestyle=ls )
      # plt.plot((qp+1)/2,0*qp,'ko')

    # plot quadrature points
    qpoints = qp_element(i_element+1,x,qp)
    plt.plot( qpoints[0 ],0, 'ks' )                   # start point
    plt.plot( qpoints[1:-1],0*qpoints[1:-1], 'ko' )   # center points
    plt.plot( qpoints[-1:],0, 'ks' )                  # end point

    # Add dof numbering
    # start point (only for 1st element)
    if i_element==0:
      dof_number += 1
      ax.annotate(str(dof_number), xy=(qpoints[0], dof_y_position),xycoords='data',
            horizontalalignment='center', verticalalignment='center',bbox=dict(boxstyle="round", fc="w")
            )

    # end point
    if (periodic and i_element==num_elements-1):
      ax.annotate(str(1), xy=(qpoints[-1], dof_y_position),xycoords='data',
          horizontalalignment='center', verticalalignment='center',bbox=dict(boxstyle="round", fc="w")
          )
    else:
      dof_number += 1
      ax.annotate(str(dof_number), xy=(qpoints[-1], dof_y_position),xycoords='data',
          horizontalalignment='center', verticalalignment='center',bbox=dict(boxstyle="round", fc="w")
          )

    # middle points
    for i_point in range (0,num_modes_quadrature-2):
      dof_number += 1
      ax.annotate(str(dof_number), xy=(qpoints[i_point+1], dof_y_position),xycoords='data',
          horizontalalignment='center', verticalalignment='center',bbox=dict(boxstyle="round", fc="w")
          )

#fig.canvas.draw()
plt.show()
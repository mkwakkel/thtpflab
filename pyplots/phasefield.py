import matplotlib.pyplot as plt
import numpy as np

# Get parameters
def getParam(str_option):
    if (str_option=='A'):
        # Option A
        a       = 1.0
        b       = 0
        gamma   = np.sqrt(2)
        #gamma   = 1/np.log(10)

    elif (str_option=='B'):
        # Option B
        a       = 0.5
        b       = 1.0
        gamma   = 2*np.sqrt(2)

    else:
        error('Undefined option')

    return a,b,gamma

# C function
def C(a, b, gamma,epsilon, LS,scale_factor):
    c_value = a * (b + np.tanh( (LS*scale_factor) / (gamma*epsilon) ) )
    return c_value

def gradC(a, b, gamma,epsilon, LS,scale_factor):
    gradc_value = a/((gamma*epsilon)) * (1 - np.tanh( (LS*scale_factor) / (gamma*epsilon) )**2 ) * scale_factor
    return gradc_value

def bulkEnergy(a, b, gamma,epsilon, LS,scale_factor):
    c_value = C(a,b,gamma,epsilon,LS,scale_factor)
    # bulk_energy = 3*np.sqrt(3)*epsilon/a * 1/(2*a**2*epsilon) * (( c_value - a*b ) * ( c_value - a*b + a ) * ( c_value - a*b - a ) )
    # bulk_energy = (3*np.sqrt(3))/(2*a**3) * (( c_value - a*b ) * ( c_value - a*b + a ) * ( c_value - a*b - a ) )
    bulk_energy = (3*np.sqrt(3))/(2) * (( c_value/a - b ) * ( c_value/a - b + 1 ) * ( c_value/a - b - 1 ) )
    return bulk_energy

def diffusiveEnergy_linearCompression(a, b, gamma,epsilon, LS,scale_factor):
    # linear compression: the second derivative of the LS is zero!
    c_value     = C(a,b,gamma,epsilon,LS,scale_factor)
    tanh_value  = ( c_value/a - b )
    # energy = -3*np.sqrt(3)*epsilon/a * 1/(2*a**2*epsilon) * epsilon**2 * (a**3*( tanh_value ) * ( 1 - tanh_value**2 ) / (epsilon**2)) * scale_factor**2
    # energy = -(3*np.sqrt(3))/(2*a**3) * epsilon**2 * (a**3*( tanh_value ) * ( 1 - tanh_value**2 ) / (epsilon**2)) * scale_factor**2
    energy = -(3*np.sqrt(3))/(2) * ( ( tanh_value ) * ( 1 - tanh_value**2 ) ) * scale_factor**2
    return energy

def plot_interfaces():
    epsilon = 0.2
    a       = 1
    b       = 0
    gamma   = 1/(np.log(10*np.sqrt(2)))

    LS = np.linspace(-1,1,1000)

    fig = plt.figure(figsize=(12, 12))

    plt.plot( LS, C(a,b,gamma,epsilon,LS,1), 'r--' )
    # plt.plot( LS, gradC(a,b,gamma,epsilon,LS,1), 'b--' )
    plt.plot( LS, gradC(a,b,gamma,epsilon,LS,1)*epsilon*gamma/a, 'b--' )
    plt.axvline(x = -epsilon/2, color = 'b', label = 'axvline - full height') 
    plt.axvline(x = epsilon/2, color = 'b', label = 'axvline - full height') 

    print( C(a,b,gamma,epsilon,epsilon,1) )
    print( C(a,b,gamma,epsilon,epsilon/2,1) )

    a       = 1
    b       = 0

    plt.plot( LS, gradC(a,b,gamma,epsilon,LS,1)*epsilon*gamma/a, 'b-' )
    plt.grid('on')

    fig.canvas.draw()
    plt.show()

def plotA(plt,epsilon):
    [a,b,gamma] = getParam('A')
    plot_all(plt,'A',a,b,gamma,epsilon)

def plotB(plt,epsilon):
    [a,b,gamma] = getParam('B')
    plot_all(plt,'B',a,b,gamma,epsilon)

def plot_all(plt,str_option,a,b,gamma,epsilon):
    str_epsilon = f"{epsilon:.2f}"
    title_string = 'Flat interface profile dynamics (option %s) \n interface thickness $\epsilon$=%s' % (str_option,str_epsilon)
    plt.suptitle( title_string )

    # gamma   = 1/np.log(10)

    xmin = -0.75
    xmax =  0.75

    LS      = np.linspace(xmin,xmax,500)

    scale_factors = [1/np.sqrt(2),1.0,1*np.sqrt(2)]
    name_tags     = ["linear stretch", "equilibrium", "linear compression"]

    number_of_interfaces    = np.size( scale_factors )
    num_plots_per_interface = 4

    for idx,scale_factor in enumerate(scale_factors):

        plt.subplots_adjust(top=0.90,bottom=0.075,left=0.075,right=0.925,hspace=0.15,wspace=0.15)

        # C profile
        ax = plt.subplot(3, num_plots_per_interface, 1+idx*num_plots_per_interface)
        if ( idx==0 or idx==2 ):
            plt.plot( LS, C(a,b,gamma,epsilon,LS,1), 'r--' )
            plt.plot( LS, C(a,b,gamma,epsilon,LS,scale_factor), 'b' )
        else:
            plt.plot( LS, C(a,b,gamma,epsilon,LS,scale_factor), 'r' )

        ax.grid(linestyle='-', linewidth=1)

        ax.set_xlim([xmin,xmax])
        #ax.set_xticks([-0.5,-0.25,0,0.25,0.5])

        if (idx==0):
            ax.set_title('C profile')

        ## Bulk energy
        ax = plt.subplot(3, num_plots_per_interface, 2+idx*num_plots_per_interface)
        if ( idx==0 or idx==2 ):
            plt.plot( LS, bulkEnergy(a,b,gamma,epsilon,LS,1), 'r--' )
            plt.plot( LS, bulkEnergy(a,b,gamma,epsilon,LS,scale_factor), 'b-' )
        else:
            plt.plot( LS, bulkEnergy(a,b,gamma,epsilon,LS,scale_factor), 'r-' )

        ax.grid(linestyle='-', linewidth=1)

        ax.set_xlim([xmin,xmax])
        #ax.set_xticks([-0.5,-0.25,0,0.25,0.5])
        ax.set_ylim([-2,2])

        if (idx==0):
            ax.set_title('Sharpening term')

        ## Diffusive term
        ax = plt.subplot(3, num_plots_per_interface, 3+idx*num_plots_per_interface)
        if ( idx==0 or idx==2 ):
            plt.plot( LS, diffusiveEnergy_linearCompression(a,b,gamma,epsilon,LS,1), 'r--' )
            plt.plot( LS, diffusiveEnergy_linearCompression(a,b,gamma,epsilon,LS,scale_factor), 'b' )
        else:
            plt.plot( LS, diffusiveEnergy_linearCompression(a,b,gamma,epsilon,LS,scale_factor), 'r' )

        ax.grid(linestyle='-', linewidth=1)
        ax.set_xlim([xmin,xmax])
        # ax.set_xticks([-0.5,-0.25,0,0.25,0.5])
        ax.set_ylim([-2,2])

        if (idx==0):
            ax.set_title('Diffusing term')

        # Chemical potential
        ax = plt.subplot(3, num_plots_per_interface, 4+idx*num_plots_per_interface)
        #plt.plot( LS, pf.bulkEnergy(a,b,gamma,LS) - pf.diffusiveEnergy_linearCompression(a,b,gamma,LS), 'b--' )
        if ( idx==0 or idx==2 ):
            plt.plot( LS, bulkEnergy(a,b,gamma,epsilon,LS,1) - diffusiveEnergy_linearCompression(a,b,gamma,epsilon,LS,1), 'r--' )
            plt.plot( LS, bulkEnergy(a,b,gamma,epsilon,LS,scale_factor) - diffusiveEnergy_linearCompression(a,b,gamma,epsilon,LS,scale_factor), 'b' )
        else:
            plt.plot( LS, bulkEnergy(a,b,gamma,epsilon,LS,scale_factor) - diffusiveEnergy_linearCompression(a,b,gamma,epsilon,LS,scale_factor), 'r' )

        ax.grid(linestyle='-', linewidth=1)

        ax.set_xlim([xmin,xmax])
        # ax.set_xticks([-0.5,-0.25,0,0.25,0.5])
        ax.set_ylim([-2,2])

        an1 = ax.annotate(name_tags[idx], xy=(1.1, 0.5), xycoords=ax.transAxes,
                  va="center", ha="center",rotation=90,
                  bbox=dict(boxstyle="round", fc="w"))

        if (idx==0):
            ax.set_title('Chemical potential')
import matplotlib.pyplot as plt
import matplotlib.tri as tri
#from mpl_toolkits.mplot3d import axes3d, Axes3D
import numpy as np
from itertools import compress 
import nemesis
from mesh_plot import *

fig = plt.figure(figsize=(12, 12))

# Determine the GLL points & weights
num_modes = 5
[qp,w] = nemesis.GaussLobattoLegendre(num_modes)

# Linear nodes
num_nodes = 100
x = np.linspace(-1,1, num=num_nodes, endpoint=True)
# x = qp

H = np.zeros( (np.size(x),num_modes) )
H = np.array(H)

H_amr_1 = np.zeros( (np.size(x),num_modes) )
H_amr_1 = np.array(H_amr_1)

H_amr_2 = np.zeros( (np.size(x),num_modes) )
H_amr_2 = np.array(H_amr_2)

for mode in range(0,num_modes):
  H[ :, mode:mode+1 ]       = nemesis.LagrangeGLL(mode,x,num_modes)
  H_amr_1[ :, mode:mode+1 ] = nemesis.LagrangeGLL(mode,(x-1)/2,num_modes)
  H_amr_2[ :, mode:mode+1 ] = nemesis.LagrangeGLL(mode,(x+1)/2,num_modes)

alpha = [0.1, 0.3, 0.2, 0.8, -0.4]

nodes_z = 0 * np.array(nodes_z)

dof_numbers = [[1,5,6,7,2,14,17,18,19,8,15,20,21,22,9,16,23,24,25,10,4,11,12,13,3],
               [2,28,29,30,26,8,37,38,39,31,9,40,41,42,32,10,43,44,45,33,3,34,35,36,27]]

with plt.xkcd():
  plt.suptitle('Adaptive mesh refinement: C0 basis')
  plt.subplots_adjust(top=0.925,bottom=0.05,left=0.05,right=0.95,hspace=0.1,wspace=0.1)

  #####################################
  # Subplot 1: basis in L0 element
  ax = plt.subplot(2, 3, 1, projection="3d")

  # Plot geometric elements
  plot_fem_mesh3D(ax,nodes_x, nodes_y, nodes_z, list(compress(all_edges, active_edges)),'red','-')
  active_faces = np.array( [1,0,0,0,0,0],dtype='bool' )
  plot_face_3D(ax,nodes_x, nodes_y, nodes_z, list(compress(all_faces, active_faces)) )

  # Plot modes & dof numbers
  for mode in range(0,num_modes):
    y = H[:,mode]
    plt.plot(0*x+1,(x+1)/2,y)
    ax.text(1,(qp[mode]+1)/2, 1, dof_numbers[0][4+num_modes*mode], va="center", ha="center")
  
  #####################################
  # Subplot 2: basis in upper L1 element
  ax = plt.subplot(2, 3, 2, projection="3d")
  ax.view_init(azim=ax.azim-100)

  plot_fem_mesh3D(ax,nodes_x, nodes_y, nodes_z, list(compress(all_edges, active_edges)),'red','-')

  active_faces = np.array( [0,0,0,0,1,0],dtype='bool' )
  plot_face_3D(ax,nodes_x, nodes_y, nodes_z, list(compress(all_faces, active_faces)) )

  for mode in range(0,num_modes):
    # x = qp
    y = H[:,mode]
    plt.plot(0*x+1,(x+1)/4+0.5,y)
    ax.text(1,(qp[mode]+1)/4+0.5, 1, dof_numbers[0][4+num_modes*mode], va="center", ha="center")

  #####################################
  # Subplot 3: basis in upper L1 element
  ax = plt.subplot(2, 3, 3, projection="3d")
  ax.view_init(azim=ax.azim-100)

  plot_fem_mesh3D(ax,nodes_x, nodes_y, nodes_z, list(compress(all_edges, active_edges)),'red','-')

  active_faces = np.array( [0,0,1,0,0,0],dtype='bool' )
  plot_face_3D(ax,nodes_x, nodes_y, nodes_z, list(compress(all_faces, active_faces)) )

  for mode in range(0,num_modes):
    # x = qp
    y = H[:,mode]
    plt.plot(0*x+1,(x-1)/4+0.5,y)
    ax.text(1,(qp[mode]-1)/4+0.5, 1, dof_numbers[0][4+num_modes*mode], va="center", ha="center")

  #####################################
  # Subplot 4: solution in L0 element
  ax = plt.subplot(2, 2, 4, projection="3d")
  
  active_faces = np.array( [1,0,0,0,0,0],dtype='bool' )
  plot_face_3D(ax,nodes_x, nodes_y, nodes_z, list(compress(all_faces, active_faces)) )

  plot_fem_mesh3D(ax,nodes_x, nodes_y, nodes_z, list(compress(all_edges, active_edges)),'red','-')
  plt.plot(0*x+1,(x+1)/2,H.dot(alpha),color='black')



  ax = plt.subplot(2, 2, 4, projection="3d")
  
  active_faces = np.array( [0,0,1,0,0,0],dtype='bool' )
  plot_face_3D(ax,nodes_x, nodes_y, nodes_z, list(compress(all_faces, active_faces)) )

  plot_fem_mesh3D(ax,nodes_x, nodes_y, nodes_z, list(compress(all_edges, active_edges)),'red','-')
  plt.plot(0*x+1,(x+1)/2,H.dot(alpha),color='black')

  # Set rotation angle to 30 degrees
  ax.view_init(azim=ax.azim-60)

  # Get current rotation angle
  # print (ax.azim)

  plt.show()

# print(L)
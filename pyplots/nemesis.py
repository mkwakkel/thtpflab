import numpy as np
import matplotlib.pyplot as plt 

# Function to return the n'th Legendre polynomial
# evaluated at points x (x can be a single point,
# but also an array)
def legendre( n, x ):
  Ln = np.zeros( (n+1, np.size(x)) )
  Ln[ 0, : ] = np.ones( (1, np.size(x)) )
  Ln[ 1, : ] = x
  if n > 1:
    for i in range(1,n):
      Ln[ i+1, : ] = (2*i+1)/(i+1) * x * Ln[ i, : ] - i/(i+1) * Ln[ i-1, : ]
  L = Ln[ n, : ]
  return L

# [p,w]=GaussLobattoLegendre(np)
# GaussLobattoLegendre quadrature
# num_points= number of points 
# p =quadrature points 
# w =quadrature weigth
#
# The tolerance in the Newton-iteration
# tol = 1E-14;
def GaussLobattoLegendre(num_points):
  tol     = 1E-14;
  p       = np.zeros( (num_points,1) );
  p[0]    = -1;
  p[num_points-1] = 1;

  w = np.zeros( (num_points) );

  if num_points<3:
    for i in range(0,num_points):
      L = legendre(num_points-1,p[i]);
      w[i] = 2/((num_points-1)*num_points*L**2);
    return p,w

  # These points are needed for the startvalues in the Newton-iteration
  [GLpoints,GLweights] = GaussLegendre(num_points-1);

  startvalues         = np.zeros( (num_points) ); 
  startvalues[1:num_points-1] = (GLpoints[0:num_points-2]+GLpoints[1:num_points-1]) / 2;

  # This loop executes the Newton-iteration to find the GLL-points
  for i in range(1,num_points-1):
    p[i]  = startvalues[i]
    p_old = 0
    while (abs(p[i]-p_old)) > tol:
      p_old = 1.0*p[i];
      L = legendre( num_points-1, p_old )
      Ld = LegendreDerivative(num_points-1,p_old);
      p[i] = p_old + ((1-p_old**2)*Ld)/((num_points-1)*num_points*L);

  # This loop finds the associated weights
  for i in range(0,num_points):
    L = legendre(num_points-1,p[i])
    w[i] = 2/((num_points-1)*num_points*L**2)

  return p,w

# Lagrange(n,x,N)
#   n   polynomial 0,1,...,N+1
#   x   coordinate -1=< x =< 1
#   N   polynomial order 
#
# Original code: Dorao, C.A., 2004 (Matlab)
def LagrangeGLL(n,x,N):
  y = np.ones( (np.size(x),1) )

  eps = np.finfo(float).eps

  [xp,w]  = GaussLobattoLegendre(N)
  i_start = 0;
  i_end   = np.size(x);

  L = legendre( N-1, xp[n] );
  
  for i in range(i_start,i_end):
    
    aux  = np.array( ( (N-1)*(N) *(x[i]-xp[n])* L ) )
    # print( aux,N,x[i] )
    aux1 = (x[i]-1) *(x[i]+1) * LegendreDerivative(N-1,x[i])

    if abs(aux*1e-2) < eps:
      if abs(aux1*1e-2)<eps:
        y[i] = 1
    else:
      y[i] = aux1 / aux

  y = np.array(y)
  return y

# Compute the Gauss Legendre quadrature rule for the interval [-1, 1]
#   num_points  number of quadrature
#   p           quadrature points
#   w           quadrature weigth 
#
# Original code: Dorao, C.A., 2005 (Matlab)
def GaussLegendre(num_points):
  from numpy.linalg import eig

  A = np.zeros( (num_points,num_points) )
  p = np.zeros( (num_points) )
  w = np.zeros( (num_points) )

  # This loop finds the A-matrix
  A[0,1] = 1

  if num_points>2:
    for i in range(2,num_points):
      A[i-1,i-2] = (i-1)/(2*i-1)
      A[i-1,i  ] = i/(2*i-1)
  A[num_points-1,num_points-2] = (num_points-1)/(2*num_points-1)

  # The array of the sorted eigenvalues/zeros
  eigen_values,eigen_vectors = eig(A)
  p = sorted( np.real(eigen_values) )

  temp = LegendreDerivative(num_points,p)

  # This loop finds the associated weights
  for j in range(0,num_points):
    w[j] = 2 / ( (1-p[j]**2) * ( temp[j] )**2 )

  w = np.array(w)
  p = np.array(p)
  return p,w

# This function returns the value of the n'th derivative of the 
# Legendre polynomial evaluated in the point, x.
#   n   derivative order 
#   x   coordinate 
# Original code: Dorao, C.A. (Matlab)
def LegendreDerivative( n, x ):  
  Ln = np.zeros( (n+1,np.size(x)) )
  Ld = np.zeros( (np.size(x)) )

  Ln[0,:] = np.ones( (1,np.size(x)) )
  Ln[1,:] = x

  # Ld[0,:] = np.zeros( (1,np.size(x)) )
  # Ld[1,:] = np.ones( (1,np.size(x)) )

  x = np.array(x)
  end_point_flags = ( np.abs(x)==1 )
  mask = np.array( end_point_flags )

  if ( (np.size(mask)==1 and mask) or (np.size(mask)>1 and any(mask)) ):
    y = x[ mask ]
    Ld[ mask ] = y**(n-1) * (1/2) * n * (n+1)
  
  if ( (np.size(mask)==1 and ~mask) or (np.size(mask)>1 and any(~mask)) ):
    y = x[ ~mask ]
    for i in range(1,n):
      Ln[i+1,~mask] = (2*i+1)/(i+1)*y*Ln[i,~mask] - i/(i+1) * Ln[i-1,~mask]
      # print(Ln[i+1,~mask])
    temp = n / (1-y**2) * Ln[ n-1, ~mask ] - n*y / (1-y**2) * Ln[ n, ~mask]
    
    Ld[ ~mask ] = temp

    # print(x,Ld)
  return Ld

# This method gives the matrix of the lagrange derivatives l'_j(x_i) 
# for i=1:Q+1 and j=1:P+1
#
# authors: Marcel Kwakkel, October 2017
def LagrangeDerivativeMatrix_GLL2( xq, N ):
  tolerance = 1e-14;
  
  #if nargin==1:
  #  P = Q
  
  print( np.size(xq) )

  Q = np.size(xq) - 1

  D = np.zeros( (Q+1,N) )
  GLLPointsQ = xq
  GLLPointsP = GaussLobattoLegendre( N )

  for i in range( 0, Q+1 ):
    xiq = GLLPointsQ[ i ][ 0 ]
    
    g       = ( 1 - xiq**2 ) * LegendreDerivative( N-1, xiq )
    gprimeQ = -N*(N-1) * legendre( N-1, xiq )
    
    for j in range ( 0, N ):
      xip = GLLPointsP[0][ j ]

      gprimeP = -(N-1)*(N) * legendre( N-1, xip )
      if ( abs(xiq-xip) < tolerance ):
        D[ i, j ] = 0
      else:
        D[ i, j ] = (gprimeQ * (xiq-xip) - g ) / (gprimeP * (xiq-xip)**2 )

  if ( xq[0]==-1 ):
    D[ 0, 0 ] = -(N-1)*N/4

  if ( xq[-1]==1 ):
    D[ -1, -1 ] =  (N-1)*N/4

  return D
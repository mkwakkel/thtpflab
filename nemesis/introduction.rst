Nemesis
*******
Nemesis is a modular framework based on the least-squares spectral element method.

.. toctree::
   :maxdepth: 3

   installation
   code-structure
   settings-file
   class-mesh
   class-field
   class-solution
   matlab-commands
   paraview
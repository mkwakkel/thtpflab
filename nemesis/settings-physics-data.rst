Block: physics_data
===================
The ``physics_data`` block is used to define the physics (equations) to be solved by Nemesis. These 
physics can be coupled, but this is not a requirement. The following listing provides an example of 
the definition of three systems (equations). The top level groups are ``systems``, ``coupling``, 
``verbose_level``. The first defines the actual physics/equations to solve for, which will be explained
in detail later. The ``coupling`` group sets the maximum number of coupling iterations, and an option could
be added (not supported at the moment) that sets which Fields are used to determine coupling convergence.
The ``verbose_level`` is a flexible option to determine the verbosity of the output, but no predefined
output options or guidelines have been implemented for this at the moment (general rule is that a lower 
value results in less output).

.. code-block:: json
   :linenos:

   {
     "physics_data" : {
       "systems" : [
       { "class_name"                        : "NavierStokes",
         "identifier"                        : 1,
         "variables"                         : ["u" "v" "p"],
         "coupled_variables"                 : ["c", "curvature" ],
         "number_of_equations"               : 3,
         "equation_weights"                  : [ 0.01, 0.01, 1 ],
         "nonlinear" : {
           "linearization" : "Newton",
           "max_iteration" : 5
         },
         "boundary_data"                   : {
           "top"       : { "type"            :"gnbc", 
                           "value"           : 0, 
                           "interface_field" : "c",
                           "velocity"        : ["u","v"],
                           "beta_f"          : "$(friction_coefficient)",
                           "Uw"              : "$(wall_velocity)",
                           "theta_0"         : "$(equilibrium_contact_angle)",
                           "Cn"              : "$(Cahn_number)",
                           "Ca"              : "$(Capillary_number)",
                           "alpha"           : "$(alpha)",
                           "gprime"          : "@switching_function",
                           "weight"          : 100
                         },
           "bottom"    : { "type"            :"gnbc", 
                           "value"           : 0, 
                           "interface_field" : "c",
                           "velocity"        : ["u","v"],
                           "beta_f"          : "$(friction_coefficient)",
                           "Uw"              : "-$(wall_velocity)",
                           "theta_0"         : "$(equilibrium_contact_angle2)",
                           "Cn"              : "$(Cahn_number)",
                           "Ca"              : "$(Capillary_number)",
                           "alpha"           : "$(alpha)",
                           "gprime"          : "@switching_function",
                           "weight"          : 100
                         }
         },
         "residual_fields"                   : [ "residual_NS" ],
         "properties"                        : {
           "pressure_gradient"     : [ 0, 0 ],
           "epsilon_div_pressure"  : 0.00
         },
         "mesh_identifier"                 : 1,
         "verbose_level"                   : 0
       },
       { "class_name"                        : "CahnHilliard",
         "short_name"                        : "CH",
         "identifier"                        : 2,
         "potential_type"                    : "balanced",
         "variables"                         : [ "c", "omega", "kappa" ],
         "coupled_variables"                 : [ "u", "v" ],
         "residual_fields"                   : [ "residual_CH" ],
         "number_of_equations"               : 2,
         "equation_weights"                  : [ 1, 1 ],
         "mobility_type"                     : 1,
         "explicit_divergence"               : false,
         "nonlinear" : {
           "linearization" : "Newton",
           "max_iteration" : 5
         },
         "mesh_identifier"                 : 1,
         "verbose_level"                   : 0
       },
       { "class_name"                        : "EnergyEquation",
         "short_name"                        : "EE",
         "identifier"                        : 3,
         "variables"                         : ["T"],
         "coupled_variables"                 : ["u","v","c"],
         "number_of_equations"               : 1,
         "equation_weights"                  : [ 1 ],
         "residual_fields"                   : [ "residual_E" ],
         "mesh_identifier"                   : 1,
         "verbose_level"                     : 0
       }],
       "coupling" : {
         "max_iteration" : 5
       },
       "verbose_level" : 0
     },
   }

The ``systems`` group is a block array in which each physics/equation is added within curly brackets. 
Each of these has a similar structure, so only the first one (``NavierStokes``) will be explained here 
in detail. The 

==============================  =========================================================================================================================================
keyword                         description
==============================  =========================================================================================================================================
``class_name``                  Name of the m-file that controls/defines this physics (the spatial dimension of the mesh will be used to select equation dimension)
``variables``                   Block array with variable names to solve for (must be defined in ``field_data``)
``coupled_variables``           Block array with variable names to use for coupling (must be defined in ``field_data``)
``number_of_equations``         Number of equations defining the physics (it allows to select different combinations of equations)
``equation_weights``            Block array with weights for each equation (size must match ``number_of_equations``)
``nonlinear``                   Defines the settings for nonlinear equations (``linearization`` method and ``max_iteration``)
``boundary_data``               Boundary data for the physics, see the note below.
``residual_fields``             Fields to use to store the residual of this physics (must be defined in ``field_data``)
``properties``                  Additional properties required for this physics only (optional, depends on the physics)
``mesh_identifier``             Identifier of the Mesh object to solve this physics on (see the note below)
``verbose_level``               Control the verbosity of the output of this physics
==============================  =========================================================================================================================================

.. important::
   In general ``boundary_data`` (boundary conditions) will be set on the Fields directly (see :ref:`Block: field_data` for more details how). 
   However, if a boundary condition depends on multiple Fields (like the Generalized Navier Boundary Condition, GNBC) this can't be used.
   In that case the boundary condition (for the Patch it works on) must removed from the Fields, and be replaced by a boundary condition
   for the Physics. These boundary conditions are added under ``boundary_data``. The general structure of a boundary conditions remains the
   same as used for a Field, but more detailed parameters can be passed to the boundary condition. 

.. note::
   Internally Nemesis will call a method within the Physics class with the name ``bc_TYPE``, where ``TYPE`` is the ``type`` value given in the
   structure. In the given example from above: the ``type`` is set to ``gnbc``. Since this boundary condition is defined within the 
   ``NavierStokes`` group, Nemesis will call the ``NavierStokes.bc_gnbc( patch_name, bc_data )`` method. This method is the actual 
   implementation of the Generalized Navier Boundary Condition (GNBC). The expected ``patch_name`` is the corresponding patch name 
   (boundary name) the condition works on (here ``top`` and ``bottom``), and ``bc_data`` is the full structure as defined
   as the value of the patch name. Here ``bc_data`` will be the structure defined by ``boundary_data.top`` and ``boundary_data.bottom``).
   This design makes it quite easy to access anything from the JSON in the actual boundary condition.

.. note:: 
   For most operations the ``identifier`` variables are not really used (only a single identifier is supported). However, theoretically 
   it is possible to solve different physics on different meshes (which might lead to interpolation). Or one could use different 
   finite elements for different meshes, or different finite elements within a single mesh. Although most of these specialized options 
   are not supported at the moment, identifiers might come in handy if someone is will to invest time in the implementations.

.. toctree::
   :maxdepth: 3

.. include:: ../links.txt
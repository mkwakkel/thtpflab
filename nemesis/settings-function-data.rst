Block: function_data
====================
The ``function_data`` block is used to define mathematical functions that can be used to initialize 
and set exact solution of Fields, see the following listing:

.. code-block:: json
   :linenos:

   {
     "function_data" : {
       "droplet" : {
          "identifier" : 1,
          "class"      : "Interface",
          "parameters" : {
            "shape"  : "circle",
            "center" : [ 0.0, 0.0 ],
            "radius" : [ 1.0 ],
            "omega"  : "balanced"
          }
       },
       "exact" : {
         "identifier"  : 2,
         "class"       : "UserDefinedFunction",
         "parameters"  : {
           "u"   : "-sin(pi*x/2) .* cos(pi*y/2) .* cos(2*pi*t)",
           "v"   : " cos(pi*x/2) .* sin(pi*y/2) .* cos(2*pi*t)",
           "p"   : "zeros(x)",
           "rhs" : "exp( -( x.^2+y.^2 ) / $(problem_data.physical_properties.sigma)^2 )"
         }
       }
     }
   }

Here the names ``droplet`` and ``exact`` are the function names, which can be used to as a reference to
that specific function (by ``@function_name``, see :ref:`Block: field_data`). The ``identifier``
is another way to reference the function, and should be a positive integer value. Next, ``class`` defines
which Matlab m-file class (located in ``$(nemesis_root)/src/functions``) must be used to process the parameters. 
The ``Interface`` class provides support for the variables ``c``, ``omega``, ``kappa`` and ``pressure``, which are all used 
by the Navier-Stokes/Cahn-Hilliard method. For droplets ``circle``/``circular`` is used, and for 
ellipsoids ``ellipse``/``ellipsoid``. Both require parameter values for the ``center`` and ``radius``.
Furthermore, ``flat`` interfaces require an additional parameters values for a ``point`` and ``normal`` vector.

.. note::
   The number of elements of ``center``, ``point`` and ``normal`` must correspond with the (spatial) mesh dimension.
   For ellipse shaped interfaces a radius value in each spatial direction is expected.

Furthermore, depending on the chosen Cahn-Hilliard method (see :ref:`Block: physics_data`), 
``omega`` can be set. Valid options are ``classic`` (which leads to a non-uniform initial chemical 
potential, the traditional definition), ``balanced`` (which leads to an improved mass conservation, 
`Kwakkel et al. (2020)`_), ``zero`` (uniform :math:`\omega = 0`), ``one`` (uniform :math:`\omega = 1`), 
or ``laplacianC`` (:math:`\omega = \Delta C`).

More general functions can be constructed by the ``UserDefinedFunction`` class, where spatial coordinates
(``x``, ``y``, ``z``) and time (``t``) can be used as variables. Furthermore, parameters defined elsewhere 
in the ``json`` file can be referenced by their position in the structure. For example, if a parameter ``sigma``
is defined under ``problem_data.physical_properties``, it can be referenced by ``$(problem_data.physical_properties.sigma)``.
Nemesis will automatically replace ``$(xyz)`` by the specific parameter ``xyz``. The ``parameters`` of the 
``UserDefinedFunction`` correspond to the variable provided by this function. For example, the given
listing will provide ``u``, ``v``, ``p``, and ``rhs``. If the ``Field`` of variable ``u`` uses ``@exact``,
internally the function defined for ``u`` will be used: :math:`u = -sin(\pi x/2) \cdot cos(\pi y/2) \cdot cos(2\pi t)` will be used.

.. caution::
   It must be assured that the mathematical operators in the defined functions are valid `Matlab operators`_.
   The variables ``x``, ``y``, ``z`` and ``t`` are arrays, and element-wise operators are therefore expected.

.. toctree::
   :maxdepth: 3

.. include:: ../links.txt
Block: solver_data
==================
The ``solver_data`` block is used to define the specifics of the solver that will be
used. This block will be passed to the constructor of the ``LeastSquaresSolver`` class,
or when the ``solve`` method of that class is called.

.. code-block:: json
   :linenos:

   {
     "solver_data" : {
       "solver_method"           : "direct",
       "reorder_method"          : "none",
       "direct_settings" : {
         "least_squares_method"  : "QR"
       },
       "iterative_settings" : {
         "method"                : "pcg",
         "use_mex"               : false,
         "convergence_criterion" : 1e-12,
         "max_iteration"         : 1000,
         "preconditioner"        : {
           "method"    : "ichol",
           "type"      : "nofill",
           "michol"    : "off",
           "droptol"   : 1e-2,
           "diagcomp"  : 1.0,
           "shape"     : "lower"
         },
         "verbose_level"         : 0
       },
       "remove_zero_columns"     : true,
       "verbose_level"           : 0
     }
   }

The ``solver_method`` can be set to ``direct`` or ``iterative``. If ``direct`` is used, the 
``direct_settings.least_squares_method.`` can be set to ``NE`` or ``QR``. Here ``NE`` indicates
that the normal equations will be created, and ``QR`` indicates that so-called direct minimization
will be used (solved by a QR decomposition of the matrix). 
For more details on the two approaches, see :cite:`hoitinga_direct_2008`. The 
``reorder_method`` might speed up the solving of the system of equations. For more details on the
valid options (``none``, ``colamd``, ``symamd``, ``symrcm``) see `Matlab reorder options`_.
If an ``iterative`` solver is selected, the ``iterative_settings`` must contain all settings of 
the iterative solver. Typical properties to set are ``method`` (default is ``pcg``, the 
`Preconditioned Conjugate Gradients method`_), ``convergence_criterion``, the 
maximum number of iterations (``max_iterations``), and the preconditioner to use. For the latter,
see the documentation of Matlab on `iterative methods for linear systems`_. The possible options
for the preconditioner match the ones available by default in Matlab. In general, 
``verbose_level`` allows the user to set the verbosity of the solver. 

.. note::
   The ``remove_zero_columns`` option will explicitly remove any columns in the matrix that contain 
   all zeros (only use when the solver does complain it needs this setting). 

.. include:: ../links.txt
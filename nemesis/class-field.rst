Field definition
****************
The ``Field`` class contains all solutions (of type ``Solution``) of a single variable. 
Each ``Field`` has its own ``FEHandler``, which controls the numbering of degrees of freedom and 
quadrature points. 


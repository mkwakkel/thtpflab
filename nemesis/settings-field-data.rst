Block: field_data
=================
The ``field_data`` block is used to define the variables (``Field`` objects in Matlab) and their
soltuions (``Solution`` objects in Matlab) which are represented on a mesh and are used by the 
equations (see :ref:`Block: physics_data`) to set previous values and gradients. By solving a set of 
equations, the solutions in the corresponding fields will be updated.
The following listing provides an example of the definition of two variables: ``u`` and ``residual``.

.. code-block:: json
   :linenos:

   {
      "field_data" : {
        "verbose_level" : 0,
        "initialize"    : true,
        "output"        : [ "u", "residual" ],
        "fields"        : [
          { "variable"                        : "u",
            "mesh_identifier"                 : 1,
            "fe_handler_data"                 : {
              "identifiers"             : 1,
              "shared_dofs"             : "all",
              "shared_quadrature_nodes" : false,
              "shared_finite_elements"  : true
            },
            "solution_data"                   : [ { "type" : "initial", "value" : 0 },
                                                  { "type" : "exact"  , "value" : "@exact" },
                                                  { "type" : "error"  , "value" : 0, "init_with_bc": false } ],
            "boundary_data"                   : {
              "fluid"     : { "type":"Internal" , "value": ["xx","yy"], "weight" : [100,100] },
              "left"      : { "type":"Dirichlet", "value":"@exact", "weight" : 100 },
              "right"     : { "type":"Dirichlet", "value":"@exact", "weight" : 100 },
              "top"       : { "type":"Dirichlet", "value":"@exact", "weight" : 100 },
              "bottom"    : { "type":"Dirichlet", "value":"@exact", "weight" : 100 }
            },
            "monitor_data" : { "filename"     : "u_data.dat",
                               "write_header" : true,
                               "variables"    : [ "timestep", "time", "error.norms.L2", "number_of_dofs" ] }
          }, 
          { "variable"                        : "residual",
            "mesh_identifier"                 : 1,
            "fe_handler_data"                 : {
              "identifiers"             : 1,
              "shared_dofs"             : "none",
              "shared_quadrature_nodes" : false,
              "shared_finite_elements"  : true
            },
            "solution_data"                   : [ { "type" : "initial", "value" : 0 } ]
          }
        ]
      }
   }

Here ``verbose_level`` allows to select a specific (debug) verbosity during the use of the fields. Next, 
``initialize`` defines if the fields need to be initialized (default is ``true``). The variable names
defined by ``output`` will be exported to a Paraview VTK file during runtime. The most important part of 
the ``field_data`` block is the ``fields`` block, which is an array (square brackets) of variables to be 
defined. Each independent field is defined between curly brackets. Detailed specifications of ``fields``, 
``solution_data``, ``boundary_data``, and ``monitor_data`` are given below.

fields specification
++++++++++++++++++++
A ``Field`` object represent a variable, and internally it is defined on a mesh (depends on a mesh object)
and one (or more) finite elements (finite element object(s)). A ``FiniteElement`` is the mapping between 
physical and natural domain (standard element domains ranging between -1 and +1). By default, a single 
``FiniteElement`` is used within each ``mesh_element``, but any support for multiple finite elements 
(necessary for p-refinement) can be added (through the ``FEHandler`` object within the ``Field``). 
Practically, a variable can have multiple solutions of interest. Therefore, within the ``Field`` object 
multiple ``Solution`` objects are created which can represent the ``last``, ``exact``, ``error``, 
``nonlinear``, ``coupling``, ``last-n``. The ``last`` solution is the most recent solution, and at 
initialization ``initial`` will be converted to ``last`` automatically. If there is an ``exact`` solution, 
also an ``error`` solution will be defined, which is the difference between ``last`` and ``exact``. The 
``nonlinear``/``coupling`` solutions are temporary solutions, used to check if nonlinear/coupling 
convergence has occurred. Note that these temporary solution will be deleted again after the solution has 
convergence. Finally, ``last-n`` represents solutions at time level ``last-n``, which can be used for time 
stepping purposes.

.. note::
   It might be more logical/efficient to include the location of the quadrature points to the mesh object as well,
   as these locations are only present in the physical domain. All Jacobian operations could/should then be moved
   to the mesh object as well. The ``Field`` object can then be defined by a polynomial type (Lagrange, Hermite) 
   and polynomial order. A simple change of the reference mesh (quadrature order / number of quadrature points) 
   could then be used to represent/plot a solution on a more detailed mesh. The finite elements still form the 'bridge'
   between the mesh (physical domain) and the basis function weights (alpha values, stored in the Field/Solution).

============================  =============================================  ==========================  ===========================================================
Field options                 description                                    required?                   remarks
============================  =============================================  ==========================  ===========================================================
variable                      Name of the variable                           yes
mesh_identifier               ID of the underlying Mesh object               yes
periodic                      Follow the periodicity of the Mesh object      no (default is ``true``)    if the Mesh object is not periodic, this won't do anything
fe_handler_data               Detailed information on the FiniteElements     yes                         see the :ref:`fe_handler_data specification`
solution_data                 Block with Solutions (see below)               yes (at least ``initial``)  see the :ref:`solution_data specification`
boundary_data                 Boundary conditions of this Field              no                          see the :ref:`boundary_data specification`
monitor_data                  Values to export for this Field                no                          see the :ref:`monitor_data specification`
============================  =============================================  ==========================  ===========================================================

.. _fe_handler_data specification:

fe_handler_data specification
+++++++++++++++++++++++++++++
Each ``Field`` has an ``FEHandler`` object, which controls the assignment of finite elements, performs 
numbering of degrees of freedom and quadrature nodes and constructs all operators (like H, Dx, Dxx, etc., and 
also the internal boundary operators). The options for the ``FEHandler`` object of a field are defined
in ``fe_handler_data``, and its specification is as follows:

============================  =============================================  ==========================  =========================================================
fe_handler_data options       description                                    required?                   remarks
============================  =============================================  ==========================  =========================================================
identifiers                   ID(s) of the FiniteElements                    yes                         single ID or array with IDs
shared_dofs                   Which DOFs should be shared?                   no (default is ``"all"``)   valid options: ``"all"``, ``"values"``, ``"none"``
shared_quadrature_nodes       Should quadrature nodes be shared?             no (default is ``true``)    see the second note below
shared_finite_elements        Should finite elements be shared?              no (default is ``true``)    reuse a single, or make a cell array of FiniteElements
============================  =============================================  ==========================  =========================================================

.. note::
   By default all DOFs (numbering) will be shared between elements, which means for C0 basis functions that the
   values will be shared and for C1 basis functions that both the values and first derivatives will be
   shared (and the mixed derivatives). The ``values`` option will limit the sharing of DOFs for C1 basis
   functions to only the DOF controlling the value. Any derivative DOFs that would be shared with ``all``
   are now decoupled between elements. Finally, ``none`` will ensure no DOFs are shared between elements. For 
   both reduced DOF sharing methods the coupling between the elements must be ensured through internal
   boundary conditions, see :ref:`boundary_data specification`. Without proper internal boundary conditions
   the solver will probably complain with rank deficient warnings or just not work at all. If the warnings
   occur even with proper internal boundary conditions, the quadrature order might be too low.

.. note::
   Consider C0 Lagrange basis functions defined on two 1D (edge) elements A and B. At their mutual vertex 
   (point C), only the value will be continuous. This C0 continuity is achieved in a strong sense, as the 
   ID of the DOF is shared between the two elements. The first derivative at point C is generally not 
   continuous, as it can be defined as a combination of the basis functions within element A or as a combination
   of the basis functions within element B. However, one could enforce continuity by combining the basis 
   functions of both elements to define a single derivative value at point C. The latter approach is
   enabled in the json file by setting ``shared_quadrature_nodes`` to ``true``.

.. _solution_data specification:

solution_data specification
+++++++++++++++++++++++++++
A ``Field`` can contain multiple solutions (``Solution`` objects), and the specification of each
solution is as follows:

============================  =============================================  ==========================  ==============================================================
Solution options              description                                    required?                   remarks
============================  =============================================  ==========================  ==============================================================
type                          Type of the solution (string)                  yes                         ``initial`` will be converted automatically to ``last``
value                         Value to assign at initialization              yes                         It is possible to make a reference to a ``UserDefinedFunction``
init_with_bc                  Apply boundary conditions at initialization?   no (default is ``true``)    
============================  =============================================  ==========================  ==============================================================

.. _boundary_data specification:

boundary_data specification
+++++++++++++++++++++++++++
A ``Field`` object can have its own boundary conditions, which only depend on the field itself and
are the same for all (internal) solutions (although ``init_with_bc`` will allow solutions without a
direct connection to the boundary conditions). The specification of the ``boundary_data`` is as follows:

============================  ===============================================  ==========================  =========================================================
boundary_data options         description                                      required?                   remarks
============================  ===============================================  ==========================  =========================================================
patch_name                    Patch name to for the condition (variable)       yes                         The patch should be available for the mesh object
type                          Boundary condition type                          yes                         Dirichlet, Neumann (see FEBoundary.m)
value                         Value for the boundary condition                 yes                         It possible to make a reference to a ``UserDefinedFunction``
form                          Weak or strong boundary condition                no (default is ``weak``)    Strong boundary conditions are not supported currently
weight                        Weight in the LS functional (for weak bc only)   yes                         Zero weight will disable the boundary condition
============================  ===============================================  ==========================  =========================================================

In order to make a reference a ``UserDefinedFunction``, that function must provide a ``parameter`` with the variable name.
For the given example here, the ``exact`` function must have a ``parameter`` with the name ``u``, see the :ref:`Block: function_data`.
A special boundary condition type is the internal boundary condition, which only has been used so far for 2D meshes (any other
use might result in errors). The patch_name for the internal boundary condition is the ``Patch`` containing all 2D elements, in the
example given here ``fluid`` is used. The ``type`` is set to ``Internal``, which will ensure the use of the ``setInternal`` function
in the FEBoundary object. The ``value`` is a block of strings with the operators to make continuous between elements, valid
strings here are: ``value``, ``x``, ``y``, ``xx``, ``xy``, ``yy`` (given the chosen polynomial is able to provide these). Finally,
``weight`` contains a block of numeric values with the least-squares weights for each provided operator (the number of weights must
exactly match the number of operators!). In the given example, the xx-derivative, and yy-derivative between elements is enforced
in a weak sense (each has a weight of 100).

.. warning::
   For the internal boundary condition there is also a ``periodic`` option, which is set to ``false`` by default. 
   Although it might work, enabling periodicity has not been verified.

.. _monitor_data specification:

monitor_data specification
++++++++++++++++++++++++++
Output of a singular ``Field`` is controlled through the ``monitor_data`` options. These are
given by:

============================  ===============================================  ==========================  =================================================================================
monitor_data options          description                                      required?                   remarks
============================  ===============================================  ==========================  =================================================================================
enabled                       Boolean to enable / disable the monitor          no                          By default the monitor is enabled (``true``)
filename                      Filename to write to                             no                          It will be written to the ``general_data`` directory (default: $variable_data)
write_header                  Boolean to enable / disable the header           no                          By default the header will be written (``true``)
header                        Add a user defined header                        no                          If not provided, the header will contain the variable names
format                        Write variables with a specific format           no                          `Matlab formatSpec`_ is expected (for example: "%5d" or "%8.3f")
variables                     Block with variables to write to the file        yes                         These variables should be available in the object
============================  ===============================================  ==========================  =================================================================================

.. note::
   If a variable is not available in the ``Field`` object, an error will terminate the execution of Nemesis.
   Maybe add some basic try/catch support to the Monitor class (it is a superclass of the Field class).

.. toctree::
   :maxdepth: 4

.. links-placeholder

.. include:: ../links.txt
Block: problem_data
===================
The ``problem_data`` block is used for problem specific settings, see the following listing:

.. code-block:: json
   :linenos:

   {
     "problem_data" : {
      "problem_name"                      : "This is the name of the problem",
      "time" : {
        "method"                          : "BDF2",
        "start"                           : 0.0,
        "current"                         : 0.0,
        "end"                             : 2.0,
        "step_size"                       : 0.01,
        "step_number"                     : 0
      },

       "physical_properties" : {
         "sigma"                           : "sqrt(2) / 5"
       },
       "additional_properties" : {
         "use_kappa_LS_correction"         : true,
         "use_sharp_surface_tension"       : true,
         "use_sharp_density"               : true,
         "use_sharp_viscosity"             : true,
         "sharpening_method"               : "Heaviside",
         "interface_thickness"             : 0.125,
         "use_implicit_pressure"           : true
       },
       "verbose_level"                     : 0
     }
   }

The ``problem_name`` is a reference that can be chosen freely by the user, it might be used for 
output of data. The ``time`` section determines if and how time is treated. Valid options for ``method``
are: ``"Steady"`` (default, time derivatives are set to zero), ``"Euler_Implicit"``,
``"BDF2"`` or ``"SpaceTime"`` (the mesh dimension will be increased with 1, so a 2D problem will create a 3D mesh).
The ``time.start`` and ``time.end`` are the initial and final time levels, while the (automatically adjusted)
``time.current`` is the time value of the current time step. The ``time.step_size`` determines the 
number of steps required to reach the final time level. Finally, ``time.step_number`` is an automatically
adjusted counter that indicates the (integer) number of steps taken to reach ``time.current``. It should be equal
to ``time.current / time.step_size``.

.. warning::
   If the ``time`` section is omitted the ``method`` will be set to ``Steady``, so no advancement
   in time will be made.

.. note::
   As ``BDF2`` requires two previous time levels, for the first time step ``ImplicitEuler`` will
   be used. After this first time step ``BDF2`` will be used again. Also the restart file will
   store all required previous solutions in time.

.. attention::
   If a mesh is loaded from a Gmsh file, ``SpaceTime`` will add an extra dimension. The same 
   mesh file can therefore be used for space-time and any of the time stepping methods.

The next two sections,
``physical_properties`` and ``additional_properties`` are quite important as they determine the 
nondimensional parameters used within the simulation. Possible options are

=========================  ===================================  ==============
physical property          description                          default value 
=========================  ===================================  ==============
length_scale               unit length scale                          1      
velocity_scale             unit velocity scale                        1      
time_scale                 unit time scale                            1
density                    density value per phase                 [1 1] 
viscosity                  viscosity value per phase               [1 1] 
gravity                    gravity vector                           [0 0]
Cahn_number                Cahn number                               0.06
Froude_number              Froude number                              1
Peclet_number              Peclet number                             100
Reynolds_number            Reynolds number                            1      
Weber_number               Weber number                               1
sigma                      surface tension                            1
mobility                   mobility (used by CH equation)             1
Weber_number               Weber number                               1
=========================  ===================================  ==============

.. note::
   Some variables (like ``sigma`` probably) might also represent other physical 
   properties, which will in general depend on the problem under consideration.

.. toctree::
   :maxdepth: 3

.. include:: ../links.txt
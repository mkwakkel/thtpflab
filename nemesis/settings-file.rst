.. _`Settings file`:

*************
Settings file
*************
All interaction with Nemesis is done through settings files, which are in JSON-format_. Note that 
whitespace is allowed and ignored around or between syntactic elements (values and punctuation, 
but not within a string value). Four specific characters are considered whitespace for this 
purpose: space, horizontal tab, line feed, and carriage return. The JSON syntax is derived from 
JavaScript object notation syntax, for which the following rules hold:

#. Data is in name/value pairs
#. Data is separated by commas
#. Curly braces hold objects
#. Square brackets hold arrays
   
.. hint::
   The official JSON-format_ does not provide syntax for comments, but Nemesis allows comments and 
   considers everything after ``' // '`` as a comment. These comments are stripped from the JSON 
   string by the ``fileread_clean`` function, and that result is then passed on to the ``loadjson``
   function (provided by the JSONLab_ module).

The main structure of the Nemesis settings file is given in the following listing:

.. code-block:: json
   :linenos:

   {
     "nemesis" : {
     },
     "problem_data" : {
     },
     "function_data" : {
     },
     "mesh_data" : {
     },
     "field_data" : {
     },
     "physics_data" : {
     },
     "solver_data" : {
     },
     "output_data": {
     },
     "analysis" : {
     }
   }

More details on the contents each of these objects can be found in their own sections. 

After the settings file 
has been loaded by Nemesis (when the Nemesis is initialized, or by calling ``System.instance()``),
all settings can be accessed anywhere within Nemesis by using ``System.getSettings()``. This function will 
return the entire structure with all settings. More specific settings can be accessed directly by passing an extra
parameter to this function. For example: ``System.getSettings( 'problem_data.physical_parameters' )`` will only return 
this part of the whole settings structure. This way it is also possible to get just a single return value instead of 
a structure.

.. hint::
   If no settings file is passed, only the ``$nemesis_root/src/core/default_settings.json`` will be loaded. Passed 
   settings files can overload these default settings. It is possible to initialize Nemesis by passing
   multiple settings files (or structures which correspond to the organization given here), where the 
   order of them determines the final value for a specific setting (the last settings file / structure 
   is the one used).

.. include:: settings-nemesis.rst

.. include:: settings-problem-data.rst

.. _`Block: function_data`:

.. include:: settings-function-data.rst

.. include:: settings-mesh-data.rst

.. _`Block: field_data`:

.. include:: settings-field-data.rst

.. _`Block: physics_data`:

.. include:: settings-physics-data.rst

.. include:: settings-solver-data.rst

.. include:: settings-output-data.rst

.. include:: settings-analysis.rst

.. .. toctree::
..    :maxdepth: 3
   
..    settings-nemesis

.. links-placeholder
.. include:: ../links.txt
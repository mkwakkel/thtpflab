Block: analysis
===============
The analysis_data block can be used for analysis purposes. However, up to now it 
has not been used as such. Therefore, it can be left out for regular users.

.. toctree::
   :maxdepth: 3

.. include:: ../links.txt
Paraview output
***************
The Matlab function ``write_vtk`` will convert the data within Field/Solution objects
to a file format that can be read by Paraview.
By default the Paraview output will be stored in the ``vtk_data`` subdirectory. The 
only exception to this rule is the Paraview data file, which has the `PVD file format`_ 
(with a ``pvd`` extension). This file is stored in the root output directory, and functions
as the common file to open all available time step solutions within Paraview at once. 
The ``pvd`` file is a simple wrapper for all time steps, which are included by adding 
a references to single time step solutions. 

.. code-block:: xml
   :linenos:
   :caption: Example of a PVD file (solution_example.pvd).

   <?xml version="1.0"?>
   <VTKFile type="Collection" version="0.1" byte_order="LittleEndian">
     <Collection>
       <DataSet timestep="000000000.000000" part="001" file="./vtk_data/solution_example_000000.pvtu"/>
       <DataSet timestep="000000000.100000" part="001" file="./vtk_data/solution_example_000001.pvtu"/>
       <DataSet timestep="000000000.200000" part="001" file="./vtk_data/solution_example_000002.pvtu"/>
       <DataSet timestep="000000000.300000" part="001" file="./vtk_data/solution_example_000003.pvtu"/>
       <DataSet timestep="000000000.400000" part="001" file="./vtk_data/solution_example_000004.pvtu"/>
     </Collection>
   </VTKFile>

Each time step solution is stored in a
`VTK file format`_. Although there are several options here, Nemesis stores each 
solution as a parallel vtkUnstructuredGrid format. This format consists again of a 
simple wrapper (the file with the `pvtu` extension), which contains references to the 
solutions at a single time step on a single rank (the files with the `vtu` extension).

.. code-block:: xml
   :linenos:
   :caption: Example of a PVTU file (./vtk_data/solution_example_000000.pvtu).

   <?xml version="1.0"?>
   <VTKFile type="PUnstructuredGrid" version="0.1" byte_order="LittleEndian">
     <PUnstructuredGrid GhostLevel="0">
       <PPoints>
         <PDataArray type="Float32" NumberOfComponents="3" format="appended"/>
       </PPoints>
       <PCells>
         <PDataArray type="Int32" Name="connectivity" format="appended"/>
         <PDataArray type="Int32" Name="offsets" format="appended"/>
         <PDataArray type="UInt8" Name="types" format="appended"/>
       </PCells>
       <PPointData>
         <PDataArray type="Float32" NumberOfComponents="1" Name="u" format="appended"/>
         <PDataArray type="Float32" NumberOfComponents="1" Name="v" format="appended"/>
         <PDataArray type="Float32" NumberOfComponents="1" Name="p" format="appended"/>
         <PDataArray type="Float32" NumberOfComponents="1" Name="residual" format="appended"/>
       </PPointData>
       <PCellData>
         <PDataArray type="UInt32" NumberOfComponents="1" Name="element_number" format="appended"/>
         <PDataArray type="UInt8" NumberOfComponents="1" Name="element_level" format="appended"/>
       </PCellData>
       <Piece Source="solution_example_p000_000000.vtu"/>
     </PUnstructuredGrid>
   </VTKFile>

.. code-block:: xml
   :linenos:
   :caption: Example of a VTU file (./vtk_data/solution_example_p000_000000.pvtu).

   <VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">
     <UnstructuredGrid>
       <Piece NumberOfPoints="5041" NumberOfCells="4900">
         <Points>
           <DataArray type="Float32" NumberOfComponents="3" format="appended" offset="0"/>
         </Points>
         <Cells>
           <DataArray type="Int32" Name="connectivity" format="appended" offset="60496"/>
           <DataArray type="Int32" Name="offsets" format="appended" offset="138900"/>
           <DataArray type="UInt8" Name="types" format="appended" offset="158504"/>
         </Cells>
         <PointData>
           <DataArray type="Float32" NumberOfComponents="1" Name="u" format="appended" offset="163408"/>
           <DataArray type="Float32" NumberOfComponents="1" Name="v" format="appended" offset="183576"/>
           <DataArray type="Float32" NumberOfComponents="1" Name="p" format="appended" offset="203744"/>
           <DataArray type="Float32" NumberOfComponents="1" Name="residual" format="appended" offset="223912"/>
         </PointData>
         <CellData>
           <DataArray type="UInt32" NumberOfComponents="1" Name="element_number" format="appended" offset="244080"/>
           <DataArray type="UInt8" NumberOfComponents="1" Name="element_level" format="appended" offset="263684"/>
         </CellData>
       </Piece>
     </UnstructuredGrid>
     <AppendedData encoding="raw">
       # LOTS OF BINARY DATA
     </AppendedData>
   </VTKFile>

This data structure allows that each single process of a parallel process writes its
own ``vtu`` file, which are merged for use in Paraview through the ``pvtu`` file.
The filename of a ``pvtu`` file contains the time step number, and the ``vtu`` files
contain both the process id (``pXXX``, where XXX is the rank of the process) and the
time step number. Note that is also possible to open each of these files from Paraview
directly. Opening a single ``pvtu`` file would import the solution at a single time step
(for all ranks), and opening a single ``vtu`` file would import the solution at a single
time step for a single rank. The most common use is probably to open the ``pvd`` file,
as this will import the solutions for all time steps and ranks.

.. toctree::
 	:maxdepth: 3

.. include:: ../links.txt
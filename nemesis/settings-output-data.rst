Block: output_data
==================
The output_data block is used to set the specifics for output of data during the
simulation, see the following listing:

.. code-block:: json
   :linenos:

   {
     "output_data": {
       "verbose_level"             : 1,
       "enable_output"             : true,
       "root_directory"            : "./two_phase_heat_transfer/",
       "general_data" : {
         "directory"               : "./post",
         "write_session"           : false,
         "session_filename"        : "session.out",
         "max_number_of_backups"   : 2
       },
       "solution_data" : {
         "format" : "paraview",
         "directory" : "./vtk_data",
         "filename" : "solution_$(problem_data.problem_name)_mesh$(mesh_id)",
         "write_frequency" : 1
       },
       "restart_data" : {
         "directory"           : "./restart",
         "filename"            : "restart",
         "write_frequency"     : 1
       }
     }
   }

The ``enable_output`` is the main switch to control any output. In order to generate any
output, it needs to be set to ``true``. The ``root_directory`` is the top-level output location,
and it can be used as a relative location with respect to the json file. Next there are 3
groups of data: ``general_data``, ``solution_data``, and ``restart_data``. Of these, ``general_data`` 
will contain all output like the Matlab session file and any output from monitors. In general, an
existing session file will not be overwritten by repeated use of the same json file. Each new
run will backup any existing session files with a number. To limit the number of backups, 
``max_number_of_backups`` can be used. The second, ``solution_data``, 
will contain output of the solutions, which should be in the Paraview as shown. Other data
format options can be added by the user of course. The ``write_frequency`` value will determine if 
output is requested every time step (value==1) or if output should be less frequent.
Finally, the ``restart_data`` is used to write
data files which can be used to restart a simulation. By default the restart data is included in the
json files written to the ``restart_data.directory``. All required data of fields and solutions is added
as binary data within the json. This allows the restart data to be loaded from a single file.

.. toctree::
   :maxdepth: 3

.. include:: ../links.txt
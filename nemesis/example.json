.. code-block:: json
   :linenos:

   {
     "nemesis" : {
       "version"                           : 2.0,
       "verbose_level"                     : 1,
       "parallel_environment"              : "none",
       "pool_type"                         : "local",
       "pool_size"                         : 4,
       "pool_restart_forced"               : true,
       "tolerance"                         : 1e-12,
       "sparse_dense_limit"                : 0.05,
       "reuse_jacobians"                   : true,
       "show_warnings"                     : false
     },
     "problem_data" : {
       "problem_name"                      : "Example - Poisson (thesis Keunsoo Park)",
       "physical_properties" : {
         "sigma"                           : "sqrt(2) / 5"
       },
       "additional_properties" : {
         "use_kappa_LS_correction"         : true,
         "use_sharp_surface_tension"       : true,
         "use_sharp_density"               : true,
         "use_sharp_viscosity"             : true,
         "sharpening_method"               : "Heaviside",
         "interface_thickness"             : 0.125,
         "use_implicit_pressure"           : true
       },
       "verbose_level"                     : 0
     },
     "function_data" : {
       "exact" : {
         "identifier"                      : 1,
         "class"                           : "ForcingFunction",
         "parameters"                      : {
           "u"   : "exp( -( x.^2+y.^2 ) / $(problem_data.physical_properties.sigma)^2 )",
           "rhs" : "(4 * ( x.^2+y.^2 ) / $(problem_data.physical_properties.sigma)^4 - 4 / $(problem_data.physical_properties.sigma)^2) .* exp( -( x.^2+y.^2 ) / $(problem_data.physical_properties.sigma)^2 )"
         }
       }
     },
     "mesh_data" : {
       "geometries" : [{
         "identifier"                      : 1,
         "type"                            : "octree",
         "maximum_level"                   : 0,
         "refinement_ratio"                : [ 2, 2 ],
         "refinement_conditions"           : [ { "field": "", "range": [0.1,0.9], "action": [-1,1,-1] },
                                                { "coordinate": "x", "range": [ 0.9, 1.1 ], "action": [-1,1,-1] }
                                             ],
         "mesh_refinement"                 : {
           "verbose_level"           : 0,
           "prevent_hanging_nodes"   : false
         },
         "input_type"                      : "matlab",
         "input_filename"                  : "unstructured_square_2x2_L1.msh",
         "mesh_name"                       : "channel",
         "number_of_elements"              : [ 8, 8 ],
         "x"                               : [ -2, 2 ],
         "y"                               : [ -2, 2 ],
         "periodic"                        : [ false, false ],
         "patch_names"                     : [ "x0y0", "x1y0", "x0y1", "x1y1",
                                               "left", "right", "bottom", "top",
                                               "fluid" ],
         "verbose_level"                   : 0
       }],
       "finite_elements" : [{
         "identifier"                      : 1,
         "polynomial_type"                 : [ "hermite", "hermite" ],
         "continuity"                      : [ 1, 1 ],
         "number_of_modes"                 : [ 5, 5 ],
         "quadrature_order"                : [ [6, 6], [12, 12] ],
         "refinement_ratio"                : [ 2, 2 ]
       }]
     },
     "field_data" : {
       "verbose_level" : 0,
       "initialize"    : true,
       "output"        : [ "u", "residual" ],
       "fields"        : [
       { "variable"                        : "u",
         "mesh_identifier"                 : 1,
         "periodic"                        : false,
         "fe_handler_data"                 : {
           "identifiers"             : 1,
           "shared_dofs"             : "all",
           "shared_quadrature_nodes" : false,
           "shared_finite_elements"  : true
         },
         "solution_data"                   : [ { "type" : "initial", "value" : 0 },
                                               { "type" : "exact"  , "value" : "@exact" },
                                               { "type" : "error"  , "value" : 0, "init_with_bc": false } ],
         "boundary_data"                   : {
           "left"      : { "type":"Dirichlet", "value":"@exact", "weight" : 100 },
           "right"     : { "type":"Dirichlet", "value":"@exact", "weight" : 100 },
           "top"       : { "type":"Dirichlet", "value":"@exact", "weight" : 100 },
           "bottom"    : { "type":"Dirichlet", "value":"@exact", "weight" : 100 }
         },
         "monitor_data" : { "filename"     : "u_data.dat",
                            "write_header" : true,
                            "variables"    : [ "timestep", "time", "error_norm", "number_of_dofs" ] }
       },
       { "variable"                        : "residual",
         "mesh_identifier"                 : 1,
         "periodic"                        : false,
         "fe_handler_data"                 : {
           "identifiers"             : 1,
           "shared_dofs"             : "none",
           "shared_quadrature_nodes" : false,
           "shared_finite_elements"  : true
         },
         "solution_data"                   : [ { "type" : "initial", "value" : 0 } ]
       }
       ]
     },
     "physics_data" : {
       "systems" : [ 
         { "class_name"                        : "Poisson",
           "identifier"                        : 1,
           "variables"                         : ["u"],
           "coupled_variables"                 : [ ],
           "number_of_equations"               : 1,
           "equation_weights"                  : [ 1 ],
           "residual_fields"                   : [ "residual" ],
           "mesh_identifier"                   : 1,
           "verbose_level"                     : 0
         }
       ],
       "verbose_level" : 0
     },
     
     "solver_data" : {
       "solver_method"                       : "direct",
       "reorder_method"                      : "none",
       "direct_settings" : {
         "least_squares_method"              : "NE"
       },
       "iterative_settings" : {  
         "method"                : "pcg",
         "use_mex"               : false,
         "convergence_criterion" : 1e-8,
         "max_iteration"         : 1000,
         "preconditioner"        : {
           "method"    : "ichol",
           "type"      : "nofill",
           "michol"    : "off",
           "droptol"   : 1e-2,
           "diagcomp"  : 1.0,
           "shape"     : "lower"
         },
         "verbose_level"         : 0,
         "compare_with_matlab"   : false
       },
       "remove_zero_columns"                 : true,
       "solver_type"                         : "static",
       "verbose_level"                       : 0
     },
     "output_data": {
       "enable_output"             : false,
       "general_data" : {
         "directory"               : "./post",
         "write_session"           : false,
         "session_filename"        : "session.out",
         "max_number_of_backups"   : 2
       },
       "solution_data" : {
         "format" : "paraview",
         "directory" : "./vtk_data",
         "filename" : "solution_$(problem_data.problem_name)_mesh$(mesh_id)",
         "write_frequency" : 1
       },
       "restart_data" : {
         "directory"           : "./restart",
         "filename"            : "restart",
         "write_frequency"     : 1
       },
       "value_filename"          : "values.txt",
       "valueSave"               : true,
       "valueStep"               : 1,
       "energyFilename"          : "energy",
       "figureFilename"          : "figure",
       "figureSave"              : true,
       "figureStep"              : 5,
       "error"                   : false,
       "relError"                : false,
       "mass"                    : true,
       "cm"                      : true,
       "cmv"                     : true,
       "debug"                   : false,
       "verbose"                 : false,
       "filecounter"             : 1,
       "residual_file_is_open"   : false
     },
     "analysis" : {
       "use_profiling"   : false
    }
   }
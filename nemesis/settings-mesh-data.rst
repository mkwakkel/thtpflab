Block: mesh_data
================
The ``mesh_data`` block is used to define the geometry of the computational domain and the details 
of the finite elements used. An example is given in the following listing:

.. code-block:: json
   :linenos:

   {
      "mesh_data" : {
       "geometries" : [{
         "identifier"                      : 1,
         "type"                            : "octree",
         "maximum_level"                   : 1,
         "refinement_ratio"                : [ 2, 2 ],
         "refinement_conditions"           : [ { "field"      : "c", 
                                                 "solution"   : "exact", 
                                                 "range"      : [ 0.1, 0.9 ], 
                                                 "action"     : [-1,1,-1]
                                               },
                                               { "coordinate" : "x", 
                                                 "range"      : [ 0.9, 1.1 ], 
                                                 "action"     : [-1,1,-1] 
                                                }
                                             ],
         "mesh_refinement"                 : {
           "verbose_level"           : 0,
           "prevent_hanging_nodes"   : false
         },
         "input_type"                      : "matlab",
         "input_filename"                  : "unstructured_square_2x2_L1.msh",
         "mesh_name"                       : "channel",
         "number_of_elements"              : [  8, 8 ],
         "x"                               : [ -2, 2 ],
         "y"                               : [ -2, 2 ],
         "periodic"                        : [ false, false ],
         "patch_names"                     : [ "x0y0", "x1y0", "x0y1", "x1y1",
                                               "left", "right", "bottom", "top",
                                               "fluid" ],
         "verbose_level"                   : 0
       }],
       "finite_elements" : [{
         "identifier"                      : 1,
         "polynomial_type"                 : [ "hermite", "hermite" ],
         "continuity"                      : [ 1, 1 ],
         "number_of_modes"                 : [ 5, 5 ],
         "quadrature_order"                : [ 6, 6 ],
         "refinement_ratio"                : [ 2, 2 ]
       }],
       "time_element" : {
         "polynomial_type"                 : "Lagrange",
         "continuity"                      : 0,
         "number_of_modes"                 : 4,
         "quadrature_order"                : 6,
         "refinement_ratio"                : 1
       }
     }
   }

.. note::
   The square brackets make it possible to define multiple ``geometries``, where each geometry must be enclosed within curly brackets.
   The same holds for the ``finite_elements``, where each (spatial) finite element must be enclosed within curly brackets.

The ``identifier`` is a unique reference number of the geometry (it might be used to identify a specific geometry). The ``type`` must
be ``"static"`` for static meshes (no adaptive mesh refinement), or ``"octree"`` if octree-based adaptive mesh refinement is possible.
If the latter is used, one must also define ``maximum_level``, which is an postive (or zero) integer value indicating how many refinement
levels are allowed. 

.. note::
   If the ``maximum_level`` is set to zero, no adaptive mesh refinement is used. However, the mesh object will be 
   constructed from the ``OctreeMesh`` class anyway. In principle StaticMesh and OctreeMesh (with ``maximum_level = 0``)
   must given identical results.

The ``refinement_ratio`` sets the number of child elements in each spatial direction. Default is a refinement factor of 2
in each spatial direction (which leads to a quadtree in 2D and an octree in 3D meshes). 

.. warning::
   The ``refinement_ratio`` here must correspond with the ``refinement_ratio`` of the finite element!

The ``refinement_conditions`` provide the rules where adaptive mesh refinement will occur. The regions of the mesh
that meet the set conditions will be refined up to ``maximum_level``. It is possible to define multiple rules (by 
using square and curly brackets), and each rule can be based on a field or on mesh coordinates.
In case of a field, valid options to pass are: ``field`` with the variable name of the field, ``solution``
to specify a specific solution of the field (if not specified ``last`` will be used), ``range`` to set the lower 
and upper limits for the refinement condition, and ``action`` which specifies what should happen (-1 for coarsening,
+1 for refinement) for elements where ``element_value < lower_limit`` (if refined), 
``lower_limit < element_value < upper_limit``, (if the element refinement level < maximum_level) or 
``element_value > upper_limit`` (if refined). The default is ``[-1,1,-1]``, which means that only elements within 
the range will be refined and will be coarsened again once they are outside this range.

.. tip::
   By default the field value will be used, but the variable name of the field can also be used to use gradients
   as conditions. The specific gradient can be set by including it in the variable name as follows ``u.x`` (for the
   x-gradient of the u variable), or ``p.yy`` (for the second derivative in y-direction of the p variable). To be
   able to use a variable is must be defined as a field, and to use second order gradients the underlying 
   finite elements must support that (for second order derivatives Hermite basis function must be used).

.. warning::
   Other values than ``[-1,1,-1]`` have not been tested properly, so be careful when using other settings.

.. attention::
   In a future version all options related with the refinement could/should be positioned within the ``mesh_refinement`` block.

The ``prevent_hanging_nodes`` option is still an alpha option, and used to apply a special refinement
that splits an element into 3 child elements. 

From which source the mesh is created by the ``input_type`` variable, and valid options are ``matlab``
or ``gmsh``. For the latter an ``input_filename`` must be provided, which should be a valid Gmsh file 
with the ``msh`` extension. In this case all Matlab mesh options are not required as the mesh file
will provide all necessary information. If the ``matlab`` mesh is used, the following options are possible:

=========================  =============================================  ==========================
Matlab mesh options        description                                    required?
=========================  =============================================  ==========================
mesh_name                  ``channel`` or ``circular``                    yes
equidistant                GLL element layout if ``false``                yes
x, y, z                    mesh limits in each spatial directions         yes (up to mesh dimension)
number_of_elements         mesh dimensions in each spatial direction      yes (up to mesh dimension)
periodic                   mesh periodicity in each spatial directions    yes (up to mesh dimension)
patch_names                overload the internally created patch_names    no
=========================  =============================================  ==========================

.. toctree::
   :maxdepth: 3

.. include:: ../links.txt
Solution
********
Where a field contains all basic information of one variable, the solution contains the 
basis function weights (``alpha``) and value for an individual solution type. 
Here the ``type`` property defines which solution is represented. The 
``type`` is user-defined (it is set in the settings file), and possible 
values are

=============  ===================================================================
type           description
=============  ===================================================================
exact          exact solution
last           most recent solution (solution at time level :math:`n`)
last-1         solution at time level :math:`n-1`
last-2         solution at time level :math:`n-2`
nonlinear      backup of last solution (used to check for nonlinear convergence)
coupling       backup of last solution (used to check for coupling convergence)
error          difference between last and exact solution
residual       residual of the least-squares system (:math:`r = b - A x`)
=============  ===================================================================

.. "attention", "caution", "danger", "error", "hint", "important", "note", "tip", "warning", "admonition"

.. note::
   The ``error`` solution is computed automatically if there is an ``exact`` solution as well.



.. :caption: Contents:

.. References
.. ==========

.. toctree::
 	:maxdepth: 2

.. 	LeastSquaresMethod
..  	BasisFunctions
.. 	NumericalIntegration
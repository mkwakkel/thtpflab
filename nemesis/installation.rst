Installation
************
The installation of Nemesis is achieved by the following 3 steps:
  1) Clone the Nemesis repository: ``git clone git@bitbucket.org:mkwakkel/nemesis.git``
  2) Move into the ``nemesis`` directory, and clone the submodules: ``git submodule update --init --recursive``
  3) Modify ``startup.m`` so that it points to the Nemesis root directory, and make sure
     MATLAB is able to find this ``startup.m`` file during startup. On Linux (Ubuntu) 
     this location is probably the home (``~/``) or in a special MATLAB directory: 
     ``~/Documents/MATLAB/``. NB: The file/location used can be determined by calling 
     ``which startup`` within MATLAB.

Next, it should be possible to run the ``Nemesis`` command at the MATLAB prompt. This should 
show some basic information and the available examples.
@startuml

    scale 1.2
    skinparam shadowing false
    skinparam defaultTextAlignment left
    skinparam activity {
      Fontcolor               white
      BackgroundColor         #2c95dd
      BorderColor             black
      ArrowColor              black
      ArrowFontColor          black
      ArrowFontSize           14
      DiamondFontColor        white
      DiamondBackgroundColor  #2c95dd
      DiamondBorderColor      black
    }
    skinparam swimlane {
      BorderThickness  0
      BorderColor      white
      TitleFontColor   hidden
    }
      
   partition PhysicsCollection.solve {

   partition MeshCollection.updateAMR {

   partition System.loadSettings {
   :Read json settings file;
   
   note right
     Examples are also loaded 
     from their own json files.
   end note
   
   	if (Valid json file?) then (no)
   		stop
   	else (yes)

	endif
    
   }
   
   :Initialize parallel environment;
    
   note right
     Currently parallel computation is 
     not supported, therefore all default 
     MATLAB functions can be used.
   end note
   
   :Create output directories;
   
   partition System.initSimulation {
      
   :**Initialize the object collections**
   # FunctionCollection
   # MeshCollection
   # FECollection
   # FieldCollection
   # PhysicsCollection;
   
   :Set Fields derived from Physics;
   
   note right
    Example: the pressure field depends 
    on the interface field (curvature).
   end note
   
   }
   }
   }
   
   if (execution_mode) then (post)
   	 stop
     note left
       Not implemented yet, but it 
       can be used for postprocessing.
     end note
   else (solve)
   partition Nemesis.solve {
     while (t < T_final OR ~FieldCollection.isSteadyState) is (true)
       :System.advanceTime;
       :PhysicsCollection.solve;
       :System.writeRestart;
       :FieldCollection.writeParaview;
       :FieldCollection.writeMonitorData;
       :System.printMemory;
     endwhile (false)
     stop
     }
   endif
      
@enduml
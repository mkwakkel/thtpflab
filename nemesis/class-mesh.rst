Mesh definition
***************

Geometric elements
==================
The geometric components of a mesh can be subdivided by their dimension: vertices, edges, faces and hexes.
A ``GeometricVertex`` is a 0D element which has a certain location in space. A ``GeometricEdge`` is a 1D
element which is defined by 2 vertices. It has a length and direction, and therefore has a mapping function
between a natural coordinate (defined as a value between -1 and 1) and the physical coordinate. The default
shape is linear, but more general shapes can be implemented (by defining a corresponding mapping function). A 
``GeometricFace`` is a 2D element, which is defined by 4 edges (in a circular order). The default shape is a 
flat plane, but a proper mapping function curved planes

A face also
has a mapping function from the natural to physical domain, and this mapping is derived from the mappings
of the 4 edges. Finally, the ``GeometricHex`` is a 3D element, which is defined by 6 faces. 

.. warning::
   Up to now 3D meshes (required for 3D (steady or with time stepping) or 2D space-time problems) 
   have not been used frequently, and any use of 3D meshes will probably result in many runtime errors.

Geometric mesh
==============
A general mesh consists of a collection of ``GeometricElements``, and the construction of these geometric
elements is handled by the ``GeometricMesh`` class. The contructor of the ``GeometricMesh`` assigns the 
``mesh_data`` variable, which is obtained from the ``InternalMesh`` or the ``Gmsh_io`` class. The first
of these is a Nemesis/MATLAB mesh definition , while the latter is obtained from a Gmsh input file (
with a ``msh`` extension). In both cases, the ``mesh_data`` variable will contain sufficient information
for the ``GeometricMesh`` class to contruct the geometric elements (``initGeometricElements`` function). 
Useful mesh object function are ``plot`` and ``plotLevels``, which plot the active geometric elements and
all geometric elements by their levels, respectively. More details on the meaning of ``active elements`` and 
``element levels`` can be found in the explanation of adaptive mesh refinement.

Mesh elements
=============
Mesh elements can be of type ``StaticElement`` or ``OctreeElement``, and are the top dimension elements
of the mesh. Their actual dimension depends on the underlying mesh dimension, therefore a mesh element can 
represent any dimension. The main difference between a ``StaticElement`` and an ``OctreeElement`` is that
the latter can be refined. 

Static / Octree mesh
====================
Although an ``OctreeMesh`` object without refinement (maximum level is identically 0) is essentially 
a ``StaticMesh``, constructing a mesh explicitly as a ``StaticMesh`` is different. The ``OctreeMesh``
class overloads certain functions from the ``StaticMesh`` class, which might in turn affect the efficiency of certain
mesh operations. Note that simulation results should always be identical. If this is not the case there
must be a bug in one of the implementations.

Mesh refinement
===============
An example of the number of the geometric elements (vertices, edges, faces, hexes) is shown for a 2D
mesh in the first figure below. The second figure shows the numbering of dofs for the same 2D mesh.

.. plot:: ./pyplots/mesh_element_numbering.py

   Example of the (geometric) element numbering for a 2D mesh with 2 level 0 mesh elements.

.. plot:: ./pyplots/mesh_dof_numbering_C0.py

   Example of the dof numbering for a 2D mesh of 2 mesh elements.
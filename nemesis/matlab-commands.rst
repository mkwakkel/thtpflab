Useful Matlab functions
***********************
Although the results of Nemesis can be written to a file format that can be opened
by Paraview, it can also be convenient to do some analysis from within Matlab. Below
the most useful Matlab functions are gathered by object class, which can be used as 
a quick reference. Unless stated otherwise, the function must be called on an instance
of the specified object class.

Settings
========
All settings are loaded by reading a JSON file, which has a structure as explained in :ref:`Settings file`. 
Within MATLAB all these settings are stored as a struct, which can be requested at any time via ``System.getSettings()``.
As it is more probable that not all settings are required at once, it is also possible to request a specific
field from the structure. For example, ``System.getSettings( 'problem_data.physical_properties' )``
will return everything defined within this field in the JSON file. 

There are two special uses of this ``getSettings`` method. First, ``System.getSettings( 'time_step_range' )``
will return an array with all time step numbers. Second, all properties defined under 
``problem_data.physical_properties`` and ``problem_data.additional_properties`` can be accessed directly.
To give an example of this (assuming ``problem_data.physical_properties.Reynolds_number`` exists): 
``System.getSettings( 'problem_data.physical_properties.Reynolds_number' )`` is identical to 
``System.getSettings( 'Reynolds_number' )``.

Field
=====
plotting
--------
``field.plot('solution_type',ST,'value_type',VT)`` will plot the value or derivative of a specific 
Solution. The solution type ``ST`` is by default equal to ``last``, but it can take any of the available Solutions
(like ``exact``, ``nonlinear``, ``coupling``, etc.) within the Field. Available solution types of a 
specific Field are stored in ``field.solution_types``.
The value type ``VT`` is by default equal to ``value``, but it can take any of the available derivatives
(like ``x``, ``y``, ``xx``, ``xy``, etc.).
Examples: 

==========================================================  ===============================================================================
Command                                                     Description
==========================================================  ===============================================================================
``u.plot('solution_type','exact','value_type','y')``        plot the y-derivative of the exact solution of the ``u`` Field object
``v.plot('value_type','xx')``                               plot the xx-derivative of the 'last' solution of the ``v`` Field object
``p.plot('solution_type','exact')``                         plot the value of the exact solution of the ``p`` Field object
==========================================================  ===============================================================================

.. note::
   Internally the arguments are converted into a ``struct``, and it is also possible to use something 
   like ``u.plot( struct('solution_type','exact','value_type','y') )`` directly.

vector with (derivative) values
-------------------------------
To return a vector with (derivative) values at the quadrature points of the Field, one can simply
use ``field.value``, ``field.x``, ``field.xx``, ``field.xy`` etc. This will return the corresponding
(derivative) values for each (active) quadrature point.

quadrature information
----------------------
All information related to the quadrature points (like physical locations, or numbering) can be 
accessed through ``field.quadrature``, which is internally a call to the quadrature information 
from the FEHandler of the Field. The returned ``struct`` contains 

==========================================================  =======================================================================================
quadrature struct field names                               Description
==========================================================  =======================================================================================
``last``                                                    The last used quadrature number
``external``                                                The number (id) of each quadrature point
``dimensions``                                              The quadrature point dimension (is the quadrature located on a Vertex, Edge, etc.)
``natural_nodes``                                           The natural nodes (ranges within -1 and +1) within each element
``physical_nodes``                                          The physical nodes (mesh locations)
``periodic``                                                Contains quadrature nodes that are periodic
``type``                                                    The quadrature point dimension for the whole mesh (see the third note below)
``Jacobian``                                                Detailed information on the Jacobians per quadrature point (see the fourth note below)
==========================================================  =======================================================================================

.. note::
   Some structure fields contain an ``array`` and ``elemental`` version. The latter is the specific information per element, 
   where the first is the merged version for the whole mesh.

.. note::
   It's probably better to rename ``external`` to ``numbering``, this naming is only a relic of an older Nemesis version.

.. note::
   It appears ``type`` can be removed, as it is identical to ``dimensions.array``. Not sure how/why this is the case.

.. note::
   For an unstructured mesh (where elements can have non-orthogonal edges/faces), the Jacobian is not constant within the
   whole element but depends on the physical location. In this general case, each quadrature point must therefore store 
   its own Jacobian information. The ``quadrature.Jacobian`` is a cell array, where each cell represents a mesh element.
   Within each mesh element cell, another cell array with two elements is stored which represent the first and second 
   derivative Jacobian information, respectively. The second derivative Jacobian information is required to calculate
   correct second order derivatives for deformed elements.

degree of freedom information
-----------------------------
All information related to the degrees of freedom (dofs) can be 
accessed through ``field.dofs``, which is internally a call to the dofs information 
from the FEHandler of the Field. The returned ``struct`` contains 

==========================================================  =======================================================================================
dofs struct field names                                     Description
==========================================================  =======================================================================================
``last``                                                    The last used dof number
``start``                                                   The start dof number (numbering starts from here)
``external``                                                Dof numbering
``levels``                                                  Dof AMR levels within each mesh element
``total``                                                   Total number of dofs
``rank_array``                                              Array with dof rank numbers (can be used for parallel computing)
``total_local``                                             Total number of dofs on this rank
``ranks``                                                   Array with dof rank numbers? (redundant?)
``shared``                                                  Cell array (size nproc) with ranks that controls the dofs (for parallel computing)
``internal``                                                Dof numbering (redundant?)
``local``                                                   Currently empty, move local dof numbering here (instead of external/internal fields)?
``scaling``                                                 Scaling weights for the dofs
==========================================================  =======================================================================================

.. note::
   As is the case with the quadrature structure, also the dofs structure contains some redundant/duplicate information.
   Main cause of these is probably the limited discussion on implementational aspects with others, and actual use of the current
   code by others. A thorough documentation and analysis of the stored data might reduce and clarify the implementation. Therefore,
   for future development it is best to have a more organized/controlled design and testing cycle during development.

Mesh
====
plotting
--------
``mesh.plot(options)`` will plot the mesh elements (upto which dimension depends on the mesh dimension): 
GeometricVertices, GeometricEdges, GeometricFaces, and GeometricHexes. The optional ``options`` is a struct
which can adjust specific plot options like:

========================  ==========================================================  ==========================
options                   Description                                                 Default value
========================  ==========================================================  ==========================
clear_figure              Clear the figure before plotting                            true
visible                   Plot all elements (not only active)                         false         
scaling                   Scale factor for edges, faces and hexes                     0.8
colors                    Colors for different elements                               {'r','b','g','c','m'}
mesh_dimension            Maximum dimension to use                                    dimension of the mesh
plot_elements             Plot vertices, edges, faces, and hexes?                     [true;true;true;false]
color_type                Variable to use to set the color of an element              'dimension'
plot_edge_order           Plot vectors indicating the order of the edges              false
plot_natural_directions   Plot vectors indicating the natural direction of the edges  false
tag_face                  Tag to use for faces                                        'id'
========================  ==========================================================  ==========================

To plot with different colors, one could create a structure with adjusted colors first: 
``plot_options.colors = {'b','g','c','m','r'}``, and next use these by passing them to 
the ``plot`` method: ``mesh.plot( plot_options )``.

plotting of active elements (OctreeMesh only)
---------------------------------------------
OctreeMesh objects have an additional plot method: ``plotActive``. This method will plot the active
mesh elements, which are the elements that provide dof numbers. For 2D meshes the AMR ``level`` of 
the GeometricElement will be used in the third direction. The result will be some kind of exploded
view of the (refined) mesh.

Patch
=====
plotting
--------
Just as the mesh, also a patch object can be plotted by calling: ``patch.plot()`` (where ``patch`` is the 
instance of a Patch object). This will plot the mesh first, and highlight the patch within the mesh as a 
black point (vertex), line (edge), or surface (plane).

.. toctree::
 	:maxdepth: 3

.. include:: ../links.txt
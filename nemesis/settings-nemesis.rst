Block: nemesis
==============
The ``nemesis`` block is used for Nemesis specific settings, see the following listing:

.. code-block:: json
   :linenos:

   {
     "nemesis" : {
       "version"                           : 2.0,
       "verbose_level"                     : 1,
       "execution_mode"                    : "solve",
       "parallel_environment"              : "none",
       "pool_type"                         : "local",
       "pool_size"                         : 4,
       "pool_restart_forced"               : true,
       "tolerance"                         : 1e-12,
       "sparse_dense_limit"                : 0.05,
       "reuse_jacobians"                   : true,
       "show_warnings"                     : false
     }
   }

The ``version`` indicates the Nemesis version, and it can be used to choose version specific methods
during a run of Nemesis. 
The ``verbose_level`` is a positive (or zero) integer value, and it is commonly used to suppress 
output if set to 0. Larger values will (in general) provide more on screen output. 
The (optional) ``execution_mode`` defines what is done with the passed settings file. Valid options 
are ``"solve"`` (default), ``"init"`` (only load the settings file), or ``"post"`` (post-processing
with already available data). 
The ``parallel_environment``
is currently not supported (only ``none`` is allowed), but it can be used in future versions to 
indicate to use the Matlab parallel pool (the ``pool_type``, ``pool_size``, and ``pool_restart_forced``
are necessary for this option) or the NMPI implementations.

.. important::
   While MPI was supported in earlier versions (and by Keunsoo Park), during the implementation of unstructred meshes 
   its support was lost. The overhead of the previous implementation was too large to be really efficient, while
   it did make everything else more complex to maintain.
   For a future re-implementation of parallel features one mainly needs to parallelize
   the ``FEHandler.m`` class. That class is used by a ``Field`` to assign the numbering of degrees of freedom (dofs)
   and quadrature points.

The ``tolerance`` is a Nemesis wide limit for nearly zero values (all values below this limit will be set to zero).
The ``sparse_dense_limit`` (a value between 0 and 1) is used to determine when a dense array should be converted 
into a sparse array. Sparse arrays are only more efficient in their use if the number of nonzeros (nnz) is low. Here
the limit is set to 5%. For efficiency, previously computed Jacobians can be reused or not (``reuse_jacobians``). 
Finally, ``show_warnings`` allows additional display of Matlab warning messages.

.. toctree::
   :maxdepth: 3

.. links-placeholder

.. include:: ../links.txt